module PriceCalculator
  
  extend ActiveSupport::Concern
  
  included do
    before_action :set_product_price_calculator
  end
  
  
  
  private
  
    def set_product_price_calculator
      calc = ProductPriceCalculator.new
      calc.account = @current_account
      calc.locale = I18n.locale
      
      Product.price_calculator = calc
    end
    
end