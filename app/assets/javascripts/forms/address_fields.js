//******************************************************************************
// This file manages all the scripts for the file views/forms/address_fields
//******************************************************************************

// switches the inputs type based on the selected country
// if the country is Italy, the province, region and city must be a <select>
// for any other country, the inputs must be of type="text"

$(function(){
  var urls = {
    getRegions: "",
    getProvinces: "",
    getCities: "/it/addresses/:id/cities"
  };

  var $address = $(".address-fields");
  var currentLayout;

  if ( !$address.length )
    return;


  // city elements
  var city = {
    $inputWrapper: $address.find(".city"),
    $selectWrapper: $address.find(".city-select")
  };
  city.$select = city.$selectWrapper.find("select");
  city.$input = city.$inputWrapper.find("input");

  // province elements
  var province = {
    $inputWrapper: $address.find(".province"),
    $selectWrapper: $address.find(".province-select")
  };
  province.$select = province.$selectWrapper.find("select");
  province.$input = province.$inputWrapper.find("input");
    
  // region elements
  var region = {
    $inputWrapper: $address.find(".region"),
    $selectWrapper: $address.find(".region-select")
  };
  region.$select = region.$selectWrapper.find("select");
  region.$input = region.$inputWrapper.find("input");

  // country elements
  var $countrySelect = $address.find(".country select");

  // cap
  var $capInput = $address.find(".cap input");


  // bind the layout change on the the country select
  $countrySelect.change(function(e){
    var val = $(this).val();

    if ( val === "Italy" || val === "Italia" ){
      setItalianInputs();
      // force an update on the cities
      province.$select.trigger("change");
    } else {
      setForeignInputs();
    }
  });


  // shows the italian inputs
  function setItalianInputs(){
    if ( currentLayout === "italian")
      return;

    // hides and disables foreign inputs
    city.$inputWrapper.hide();
    province.$inputWrapper.hide();
    region.$inputWrapper.hide();    

    city.$input.attr("disabled", "disabled");
    province.$input.attr("disabled", "disabled");
    region.$input.attr("disabled", "disabled");

    // enables and shows italian selects
    city.$select.removeAttr("disabled");
    province.$select.removeAttr("disabled");
    region.$select.removeAttr("disabled");

    city.$selectWrapper.show();
    province.$selectWrapper.show();
    region.$selectWrapper.show();

    currentLayout = "italian";
  }

  // shows foreigns inputs
  function setForeignInputs(){
    if ( currentLayout === "foreign")
      return;

    // hides and disables the italian selects
    city.$selectWrapper.hide();
    province.$selectWrapper.hide();
    region.$selectWrapper.hide();

    city.$select.attr("disabled", "disabled");
    province.$select.attr("disabled", "disabled");
    region.$select.attr("disabled", "disabled");
    
    // enables and shows foreign inputs
    city.$input.removeAttr("disabled");
    province.$input.removeAttr("disabled");
    region.$input.removeAttr("disabled");

    city.$inputWrapper.show();
    province.$inputWrapper.show();
    region.$inputWrapper.show();


    currentLayout = "foreign";
  }

  // binds the cities request on province change
  province.$select.change(function(){
    updateCities( $(this).val() );
  });

  // binds get cities
  function updateCities(regionName){
    if ( !urls.getCities )
      return;

    // stores the value before updating the options
    var currentValue = city.$select.val();
    city.$select.attr("disabled", "disabled");

    $.get(urls.getCities.replace(":id", regionName), function(html, statusText, xhr){
      if ( html ){
        city.$select.html( html );
        // try to select the previous value
        if ( city.$select.find("option[value=\"" + currentValue + "\"]").length )
          city.$select.val(currentValue);
      }
      city.$select.removeAttr("disabled");
      getSuggestedCap();
    }, "html");
  }



  city.$select.change(getSuggestedCap);

  // get the suggested cap for the current city
  function getSuggestedCap(){
    var cityName = city.$select.val();
    var provinceName = province.$select.val();

    if ( cityName && provinceName ){
      var url = "/it/addresses/" + provinceName + "/" + cityName + "/suggest-cap";

      $.get(url, function(html, statusText, xhr){
        // make an approximative check, we assume that the cap length cannot be longer than 10
        if ( html.length < 10 ){
         $capInput.val(html);
         $capInput.trigger("change");
        }
      }, "text")
    }
  }



  // triggers a select change to initialize the appropriate layout
  $countrySelect.trigger("change");
});