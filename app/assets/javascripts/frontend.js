// i'll split this up later
//
//= require jquery
//= require jquery_ujs
//= require popper

////////////////////////////////////////////////////////////////////////////////
// dont use packed bootstrap, use only needed module!
// for some plugin i'd rather to use jquery-ui's since they offer more
// flexibility
////////////////////////////////////////////////////////////////////////////////
//= require bootstrap/util
//= require bootstrap/alert
// require bootstrap/button
// require bootstrap/carousel
// require bootstrap/collapse
//= require bootstrap/dropdown
//= require bootstrap/modal
// require bootstrap/scrollspy
// require bootstrap/tab
// require bootstrap/tooltip
// require bootstrap/popover
////////////////////////////////////////////////////////////////////////////////
// end of bootstrap libs
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// jquery
////////////////////////////////////////////////////////////////////////////////
//= require jquery-ui/core
//= require jquery-ui/widgets/mouse
// require jquery-ui/widgets/draggable
// require jquery-ui/widgets/droppable
// require jquery-ui/widgets/resizable
// require jquery-ui/widgets/selectable
// require jquery-ui/widgets/sortable
// require jquery-ui/widgets/accordion
// require jquery-ui/widgets/autocomplete
// require jquery-ui/widgets/button
// require jquery-ui/widgets/datepicker
// require jquery-ui/widgets/dialog
// require jquery-ui/widgets/menu
// require jquery-ui/widgets/progressbar
// require jquery-ui/widgets/selectmenu
// require jquery-ui/widgets/slider
// require jquery-ui/widgets/spinner
// require jquery-ui/widgets/tabs
//= require jquery-ui/widgets/tooltip
//= require jquery-ui/position
//= require jquery-ui/effect.all
////////////////////////////////////////////////////////////////////////////////
// end of jquery
////////////////////////////////////////////////////////////////////////////////

//= require js.cookie
//= require royalslider.min
//= require jquery-imagefill
//= require hoverIntent
//= require superfish
//= require supersubs
//= require jquery.magnific-popup.min
//= require sweetalert2.min
//= require jquery-scrolltofixed-min
//= require selectize.min

$(function() {
  // $('select').selectize()
  // //
  // // init floating labels
  // //
  // $('.floating-labels input, .floating-labels textarea').floatingLabels();

  //////////////////////////////////////////////////////////////////////////////
  // mobile menu
  //////////////////////////////////////////////////////////////////////////////
  $('.mobile-nav-toggler').click(function() {
    $('.mobile-nav-overlay').fadeToggle(200)
    $('.mobile-nav-toggler').toggleClass('active')
    $('body').toggleClass('mobile-nav-opened')
  });

  $(".site-wrap").click(function(){
    if($('.mobile-nav-opened').is(":visible")){
      $('.mobile-nav-overlay').fadeToggle(200)
      $('.mobile-nav-toggler').toggleClass('active')
      $('body').toggleClass('mobile-nav-opened')
    }
  });

  // $('.overlay').on('click', function(){
  //     $(".overlay").fadeToggle(200);
  //     $(".button a").toggleClass('btn-open').toggleClass('btn-close');
  //     open = false;
  // });

  //////////////////////////////////////////////////////////////////////////////
  // imagefill (http://johnpolacek.github.io/imagefill.js/)
  //////////////////////////////////////////////////////////////////////////////
  // $('.link-bg-container .image-container').imagefill();


  //////////////////////////////////////////////////////////////////////////////
  // shopping cart popover on top menu
  //////////////////////////////////////////////////////////////////////////////

  $(document).click(function() {
    $('.add-cart-products-popup').fadeOut()
  })

  $('.nav-item.cart .nav-link').each(function() {
    $(this).click(function(e) {
      e.stopPropagation()
      $('.add-cart-products-popup').fadeToggle()
    })
  })

  $('.add-cart-products-popup').click(function(e) {
    e.stopPropagation()
  })


  //////////////////////////////////////////////////////////////////////////////
  // automatically convert tooltip to use jquery-ui's
  //////////////////////////////////////////////////////////////////////////////

  $.widget('ui.tooltip', $.ui.tooltip, {
    options: {
      content: function () {
        return $(this).prop('title')
      },
      track: true,
      position: {
        my: 'center top+16',
        collision: 'flipfit'
      }
    }
  })

  $(document).tooltip()


  //////////////////////////////////////////////////////////////////////////////
  // cookies popup
  // requires jQuery and js.cookies (https://github.com/js-cookie/js-cookie)
  //////////////////////////////////////////////////////////////////////////////

  // common variables
  var
    cookies = {},
    popup = $('#cookies-message')

  cookies.$close = popup.find('.c-close')
  cookies.$accept = popup.find('.c-accept')

  // simply close cookies popup when clicking on the "X" button
  cookies.$close.click(function(e) {
    popup.fadeOut()
  });

  // when the user accepts, close popup and set a cookie to prevent the popup
  // from reappearing
  cookies.$accept.click(function(e) {
    popup.fadeOut()
    Cookies.set("cookieconsent_dismissed", "yes", { expires: 365 })
  })


  //////////////////////////////////////////////////////////////////////////////
  // automatically change input[type="number"] into styled spinner input
  //////////////////////////////////////////////////////////////////////////////

  $inputEl = $('input[type="number"]')

  $($inputEl).each(function() {
    var el = $(this),
      spinner = el.closest('div'),
      min = parseInt(el.attr('min')) || 1,
      max = parseInt(el.attr('max')) || 100000,
      step = parseInt(el.attr('step')) || 1,
      oldVal,
      newVal

    $('<div class="input-group-prepend"><span class="input-group-text number-down">-</div></div>').insertBefore(el)
    $('<div class="input-group-append"><span class="input-group-text number-up">+</div></div>').insertAfter(el)

    var btnUp = spinner.find('.number-up'),
      btnDown = spinner.find('.number-down')

    function changeVal(action) {
      oldVal = parseInt(el.val());

      if (action == 'up' && oldVal < max && (oldVal + step <= max)) {
        newVal = oldVal + step
        el.val(newVal)
        el.trigger('change')
      } else if (action == 'down' && oldVal > min && (oldVal - step >= min)) {
        newVal = oldVal - step
        el.val(newVal)
        el.trigger('change')
      } else {
        newVal = oldVal
      }
    }

    btnUp.click(function() {
      changeVal('up')
    })

    btnDown.click(function() {
      changeVal('down')
    })
  });


  //////////////////////////////////////////////////////////////////////////////
  // automatically disable button right after it clicked
  //////////////////////////////////////////////////////////////////////////////

  var w, t;

  $.fn.disableButton = function() {
    // maintain button width & text to restore later when it's re-enabled
    w = this.outerWidth()
    t = this.val()

    // use &nbsp; instead of empty string for value
    this.css('width', w).val('&nbsp;').addClass('btn-disabled btn-loading').attr('disabled', 'disabled')
  }

  $.fn.reEnableButton = function() {
    this.css('width', '').val(t).removeClass('btn-disabled btn-loading').removeAttr('disabled')
  }

  $('form').each(function() {
    $(this).on('submit', function() {
      $submit = $(this).find('input[type="submit"]')
      $submit.disableButton()
    })

    $(this).on('ajax:complete', function() {
      $submit = $(this).find('input[type="submit"]')
      $submit.reEnableButton()
    })
  })


  //////////////////////////////////////////////////////////////////////////////
  // cart
  //////////////////////////////////////////////////////////////////////////////

  // sticky rightbar
  var $rightbar = $('.cart-page .rightbar')

  $rightbar.scrollToFixed({
    marginTop: 16,
    limit: $('.footer').offset().top - $rightbar.find('.right-products').outerHeight(true) - 16
  })

  $(window).load(function() {
    $rightbar.trigger('resize')
  })

  // toggle payments details visibility when changing radio
  $(".payment-fields .payment input[type='radio']").change(function() {
    var $payment = $(this).parents('.payment')

    $('.payment-fields .payment').removeClass('current')
    $payment.addClass('current')
  })

  $('.payment-fields .payment input:checked').trigger('change')


  //////////////////////////////////////////////////////////////////////////////
  // scroll Up
  //////////////////////////////////////////////////////////////////////////////

  $(window).scroll(function () {
    if ($(this).scrollTop() > 500) {
      $('.scrollup').stop().fadeIn();
    } else {
      $('.scrollup').stop().fadeOut();
    }
  })

  $('.scrollup').click(function () {
    $('html, body').animate({
      scrollTop: 0
    }, 600);
    return false;
  })
});
