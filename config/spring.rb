# this holds the configurations for spring

# every time a watched file changes, the application gets reloaded
Spring.watch "config/initializers"
Spring.watch "config/config.yml"
Spring.watch "app/models_decorations"