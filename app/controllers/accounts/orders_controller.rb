class Accounts::OrdersController < AccountsController

  add_breadcrumb I18n.t("order_list"), :accounts_orders_path
        

  def index
    # show all account's orders, even if they have been cancelled
    @orders = @current_account.orders.order({created_at: :desc})
    @account_action = "orders"
  end


  def show
    # show all account's orders, even if they have been cancelled
    @order = @current_account.orders.find( params[:id] )
    @account_action = "orders"
    add_breadcrumb "#{t('order_number')} #{@order.custom_id}"
  end


  def cancel
    # only the current_account's active orders can be cancelled
    @order = @current_account.orders.active.find( params[:id] )
    @order.update(cancel_params)
    redirect_to accounts_order_path(@order)
  end



  def payment
    # only the current_account's active orders can be paid
    @order = @current_account.orders.active.find( params[:id] )
    @account_action = "orders"
    @payments = @current_ecommerce.payments.publics.ordered
    add_breadcrumb "#{t('payment')} #{@order.custom_id}"
  end


  def update_payment
    @order = @current_account.orders.active.find( params[:id] )
    @order.update( params[:order].permit(:payment_id) )
    redirect_to action: "payment"
  end



  private

    def cancel_params
      params.require(:order).permit(:reason, :status_note)
    end

end