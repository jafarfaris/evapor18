#
module PriceListExt
  extend ActiveSupport::Concern

  included do
    scope :valid_products, -> { where('(from_time IS NULL OR from_time <= ?) AND (to_time IS NULL OR to_time >= ?)', Time.zone.now, Time.zone.now) }
    default_scope { valid_products }
  end
end
