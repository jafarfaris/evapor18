#
module ProductAttributeHelper
  #
  def all_attributes
    Detail.joins(:product_versions).where(product_versions: { id: @current_ecommerce.products.pluck(:product_version_id) }).uniq
  end

  #
  def attr_name(att)
    if I18n.locale.to_s == att.locale
      att.name
    else
      att.translation[I18n.locale]
    end
  end

  #
  def attr_values(att)
    if I18n.locale.to_s == att.locale
      att.value
    else
      att.translation_value[I18n.locale]
    end.split(',').map(&:strip)
  end
end
