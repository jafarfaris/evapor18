#
module ProductViewsHistory
  extend ActiveSupport::Concern

  #
  def viewed_product_ids
    cookies[:__rwp].to_s.split(',').map(&:to_i).uniq
  end

  #
  def viewed_products
    ecommerce_products.where(id: viewed_product_ids)
  end

  #
  def track!(no_counter = false)
    # sort by newer viewed item first, then we take only 10 last viewed items
    new_val_for_cookies =
      ([@product.id] << viewed_product_ids).flatten.uniq.first(10).join(',')

    # update the cookies
    cookies[:__rwp] = new_val_for_cookies

    # save viewed products to db if user logged in
    if account_signed_in?
      account_viewed_product_ids =
        current_account.recently_viewed_products_ids.to_s.split(',').map(&:to_i)
      new_val = ([@product.id] << account_viewed_product_ids).flatten.uniq
      current_account.update recently_viewed_products_ids: new_val.join(',')
    end

    # we do this the old way. should find better way to do this, like using
    # impressionist gem (?), etc
    #
    # make sure product view count doesn't count if the cookie[:__ltp] value
    # same with current product id
    if cookies[:__ltp].to_i != @product.id
      cookies['__ltp'] = @product.id
      new_val = @product.visulizations_counter + 1
      @product.update visulizations_counter: new_val unless no_counter
    end
  end
end
