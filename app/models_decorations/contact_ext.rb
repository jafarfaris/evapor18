#
module ContactExt
  extend ActiveSupport::Concern

  attr_accessor :privacy, :form_id

  included do
    default_scope -> { where({ ecommerce_id: ENV['ECOMMERCE_ID'] }) }

    # adds privacy checks
    validates_presence_of :typology

    # email, name and ecommerce_id are automatically validated for every contact typology

    # validations
    # typology-specific validations
    # contatto generico
    # validates :message, presence: true, if: ->(c){ c.contatto_generico? }
    # validates :telephone, presence: true, if: ->(c){ c.contatto_generico? }
    validate :dynamic_fields_validation

    #
    def dynamic_fields_validation
      opts = Sitemap.find(form_id).sitemap_options
      field_opts = opts.where('options LIKE ?', '%name: fields%')
      privacy_opts = opts.where('options LIKE ?', '%name: privacy%').first

      field_opts.each do |opt|
        opt.options['value'].split(';').map(&:strip).each do |field|
          field = field.split(':').map(&:strip)
          if field[1] == 'required' && !field[0].present?
            errors.add field[0].to_sym, I18n.t('contacts.required')
          end
        end
      end

      if privacy_opts.options['value'].to_s == 'true' && privacy.blank?
        errors.add :privacy, I18n.t('contacts.required')
      end
    end
  end
end
