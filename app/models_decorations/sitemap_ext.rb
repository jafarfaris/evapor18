module SitemapExt
  extend ActiveSupport::Concern

  included do
    include OgImageable
    translate_fields :text, :short_description, :description, :page_title, :meta_key, :meta_description, :link, :extra_1, :extra_2, :extra_3

    # there's no point to call non-active sitemap at the frontend
    default_scope { active.translation_active }

    def self.translation_active
      if I18n.locale != I18n.default_locale
        joins(:translations).where(generic_translations: { draft: false, locale: I18n.locale.to_s.downcase })
      else
        all
      end
    end
  end
end
