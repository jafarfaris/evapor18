#
module InternationalPriceExt
  extend ActiveSupport::Concern

  included do
    # if a price doesn't have from_time & to_time, we consider it valid forever
    # if a price have from_time & to_time, we'll querying it
    scope :valid_by_period, -> { where('(from_time IS NULL OR from_time <= ?) AND (to_time IS NULL OR to_time >= ?)', Time.zone.now, Time.zone.now) }
    scope :valid_by_location, -> { where('locale like ?', "% #{user_loc}\n%") }

    default_scope { valid_by_period.valid_by_location }

    # for easier debugging using on the console (default value is ID)
    def self.user_loc
      $_COOKIES.present? && $_COOKIES[:loc].present? ? $_COOKIES[:loc] : 'ID'
    end
  end
end
