require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

Dotenv::Railtie.load

require File.join(ENV['LIPSIASTORE_PATH'], 'lib', 'lipsiastore4.rb')

module Lipsiaecommerce
  #
  class Application < Rails::Application
    # configure the ecommerce using the settings defined in the lipsiastore4
    # application
    ::Lipsiastore4::Client.configure(config)

    # this regexp doesn't precompile partials
    # preventing the errors of random undeclared sass variables
    config.assets.precompile << /(^[^_\/]|\/[^_])[^\/]*\.(js|css|scss)$/

    # Settings in config/environments/* take precedence over those specified
    # here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record
    # auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names.
    # Default is UTC.
    config.time_zone = 'Rome'

    # Set this to english first, to avoid DB hit on this process,
    # after app started we'll set it to default language set in the lipsiastore
    config.i18n.default_locale = :en

    # Do not swallow errors in after_commit/after_rollback callbacks.
    config.active_record.raise_in_transactional_callbacks = true

    # Cleans the thread used to store the product price calculator
    config.middleware.use 'PriceCalcThreadCleaner'

    # cache management via redis
    config.cache_store = :redis_store, {
      host: (ENV['ECOMMERCES_REDIS_HOST'] || 'localhost'),
      port: (ENV['ECOMMERCES_REDIS_PORT'] || 6379),
      db: (ENV['ECOMMERCES_REDIS_DB'] || 0),
      password: ENV['ECOMMERCES_REDIS_PASSWORD'],
      namespace: (ENV['ECOMMERCES_REDIS_NAMESPACE'] || "ecommerce4:#{ENV['NAME']}")
    }

    # reload the locales here to override the lipsiastore's translations
    config.i18n.load_path += Dir[Rails.root.join('config/locales/*.{rb,yml}').to_s]

    config.sass.quiet = true
  end
end
