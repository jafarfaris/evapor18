## Models decorations

The models_decoration folder should contain the concerns used to override or expand the dafult lipsiastore4 models.
Those decorations are valid only for this application, this way each ecommerce can have its own freedom from the lipsiastore4 constraints (or can even add more).

By convention, the file should be named *modelname_ext.rb*, if this convention is respected, the file will be auto loaded and included in the model by an initializer.
If the name is different, the developer should implement the whole process manually.

few naming examples:

| model               |  decoration file          |
|---------------------|---------------------------|
| Order               |  order_ext.rb             |
| ShippingAddress     |  shipping_address_ext.rb  |
| Attachment::Image   |  attachment/image_ext.rb  |


## Translations

To enable the translation on certain fields, you can use the `translate_fields` method *inside the included block* of a model's decoration.
You can also exclude or translate only specific fields for one or more locales with the options `only: :it` or `except: [:fr, :de]`.

```
included do
  # Those fields will be translated for every locale
  translate_fields :description, :extra_1

  # The name will be read directly from the  appropriate table column, except for the chinese that will look for a translation
  translate_fields :name, only: :"zh-cn"

  # Those fields will be translated in every locale except for the german or french that will read the appropriate attribute from the product's column
  translate_fields :extra_2, :extra_3, :extra_4, except: [:de, :fr]

  # Watch out for repetitions that can lead to unexcpected results
  translate_fields :extra_5, only: :de, except: [:de, :fr]
end
```