module ShippingAddressExt
  extend ActiveSupport::Concern
  
  # updates the address of the associated shopping_carts
  def update_carts_addresses
    shopping_carts.in_progress.each do |cart|
      cart.delivery = cart.possible_deliveries.cheapest unless cart.possible_deliveries.include?(cart.delivery)
      cart.save
    end
  end

end