# this controller manages the login from the cart

class Frontend::Cart::SessionsController < Devise::SessionsController

  # # POST /resource/sign_in
  # def create
  #   self.resource = warden.authenticate!(auth_options)
  #   set_flash_message!(:notice, :signed_in)
  #   sign_in(resource_name, resource)
  #   yield resource if block_given?
  #   respond_with resource, location: after_sign_in_path_for(resource)
  # end


  def after_sign_in_path_for(resource)
    resource.can_order? ? cart_confirm_path : cart_missing_details_path
  end
  
end