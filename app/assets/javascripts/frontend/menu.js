(function(){

  var $menu = $("header#site-menu");

  var $toggleMenuMobile = $menu.find(".mobile-menu-toggler");
  var $menuOverlay = $(".site-menu-overlay");

  // lang dropdown
  // var $langMenu = $menu.find(".lang-menu");
  // var $langBtn = $langMenu.find(".toggle-langs");
  // var $langDd = $langMenu.find(".available-langs");

  // bind lang menu
  // $langBtn.click(function(e){
  //   e.preventDefault();
  //   $langDd.toggleClass("expanded");
  // })

  // // close lang menu when clicking outside
  // $(document).mouseup(function(e){
  //   if ( $langMenu.has(e.target).length === 0 ){
  //     $langDd.removeClass("expanded");
  //   }
  // })

  // bind toggle menu mobile
  $toggleMenuMobile.click(function(e){
    e.preventDefault();
    $("body").toggleClass("menu-opened");
  })

  // close menu when clicking outside the menu
  // $(document).click(function(event) {
  //   if(!$(event.target).closest('.menu-opened').length) {
  //     if($('.menu-opened').is(":visible")) {
  //       $("body").removeClass("menu-opened");
  //     }
  //   }
  // })

  // close menu when clicking on the overlay
  $menuOverlay.click(function(e){
    e.preventDefault();
    $("body").removeClass("menu-opened");
  })
})();
