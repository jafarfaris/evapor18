module PaymentHelper

  # Form per i pagamenti
  def form_payment_tag_for(order, payment)

    order_total   = number_with_precision(order.actual_total, :separator => ".", :precision => 2)
    order_integer = order.actual_total.to_i
    order_decimal = ("%.2f" % (order.actual_total.to_f - order.actual_total.to_i)).to_s.gsub("0.", "")

    if payment.paypal_pro?
      # carico il subtotal scorporando iva e spedizione
      subtotal = order.total_without_vat.to_f - order.delivery_price_without_vat.to_f
      parse_payment_code( payment.code, {
                            ##### Spedizione #####
                           :address1              => order.ship_address,
                           :city                  => order.ship_city,
                           :country               => order.ship_country,
                           :first_name            => order.ship_name.split(" ")[0],
                           :last_name             => (order.ship_name.split(" ")[1..-1].join(" ").to_s rescue ""),
                           :zip                   => order.ship_cap,
                           :buyer_email           => order.customer_email,
                           # :state               => Non abbiamo lo stato federale

                           ##### Totali #####
                           # :handling            => # Spese di imballaggio addebitate. Questo importo viene aggiunto al subtotal dell'importo totale.
                           :shipping              => number_with_precision(order.delivery_price_without_vat, :separator => ".", :precision => 2),           # Spedizione addebitata. Questo importo viene aggiunto al subtotal dell'importo totale
                           :tax                   => order.with_vat ? number_with_precision(order.total_vat, :separator => ".", :precision => 2) : "0.0",   # Imposte addebitate. Questo importo viene aggiunto al subtotal dell'importo totale
                           :subtotal              => number_with_precision(subtotal, :separator => ".", :precision => 2),                                   # Importo addebitato per la transazione. Se non vengono specificate le spese di spedizione e imballaggio e le imposte, si tratta dell'importo totale addebitato.

                           ##### Fatturazione #####
                           :billing_address1      => order.invoice_address,
                           :billing_city          => order.invoice_city,
                           :billing_country       => order.invoice_country,
                           :billing_first_name    => order.invoice_name.split(" ")[0],
                           :billing_last_name     => (order.invoice_name.split(" ")[1..-1].join(" ").to_s rescue ""),
                           # :billing_state       => Non abbiamo lo stato federale
                           :billing_zip           => order.invoice_cap,
                           :invoice               => order.custom_id,     # Fattura, non posso passarlo perchè a questo punto non è stata creata.

                           ##### Varie #####
                           :lc                    => I18n.locale.to_s,    # Lingua della pagina di accesso o di registrazione.
                           :custom                => "#{order.id}",
                           # :business            => "info@lipsiashop.it" # Glielo passo dallo store
                           :currency_code         => "EUR",
                           :cancel_return         => payment_accounts_order_url(id: order),
                           :notify_url            => "http://store.lipsiasoft.com/paypal_ipn",
                           :paymentaction         => "vendita",                                       # Indica se la transazione è un pagamento per una vendita definitiva o un'autorizzazione per una vendita definitiva (da riscuotere in seguito). Valori consentiti: autorizzazione o vendita Valore predefinito: vendita.
                           :return                => payment_accounts_order_url(id: order)
      })
    else
      parse_payment_code(payment.code, {
                         :amount                => order_total,
                         :lang                  => I18n.locale.to_s,
                         :name                  => "Pagamento ordine Numero #{order.custom_id}",
                         :code                  => order.custom_id,
                         :custom                => "#{order.id}",
                         :url_ok                => payment_accounts_order_url(id: order),
                         :url_ko                => payment_accounts_order_url(id: order),
                         :return                => payment_accounts_order_url(id: order),
                         :cancel_return         => payment_accounts_order_url(id: order),
                         :first_name            => order.invoice_name,
                         :address1              => order.invoice_address,
                         :city                  => order.invoice_city,
                         :zip                   => order.invoice_cap,
                         :email                 => order.customer_email
      })
    end
  end


  def parse_payment_code(payment_code, variables)
    parsed_code = payment_code
    variables.each { |k,v| parsed_code.gsub!(/##{k}#/i, v)  }
    return parsed_code
  end


end