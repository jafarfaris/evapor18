#
class FrontendController < ApplicationController
  include ParamsTrackable

  before_action :detect_browser, :mobile_viewport, :load_sitemaps, :set_account
  before_action :set_vacation_message, :set_tracking_services
  before_action :store_current_location, unless: :devise_controller?

  # The PriceCalulator concern must be included after the locale and the accounts have been set
  # include PriceCalculator

  # the cart should be set after the PriceCalculator concern has been loaded, to merge the cart with the appropriate prices if necessary
  before_action :set_cart

  # Breadcrumbs, adds the "home" into the whole site with the exception of the accounts controller
  add_breadcrumb I18n.t('home'), :root_path, unless: ->(c){ c.is_a?(AccountsController) }

  # errors rescued with a 404 page
  # unless Rails.env.development?
    rescue_from ActiveRecord::RecordNotFound,     with: :render_not_found
    rescue_from ActionController::RoutingError,   with: :render_not_found
  # end

  # redirects urls without params lang
  def redirect_lang
    # I18n.locale is set in the application_controller
    redirect_to "/#{I18n.locale}"
  end

  private

  #
  def render_not_found
    render 'exceptions/404', status: 404
  end

  # load common sitemaps that are used inside the layout
  def load_sitemaps
    @all_pages = @current_ecommerce.sitemaps

    @menu = @all_pages.find_by(layout: 'menu')
    included_layouts = ['homepage', 'page', 'root_category']
    @menu_pages = @menu.children.menu_enable.where(layout: included_layouts).order(lft: :asc) rescue []

    @homepage = @menu.children.find_by_layout('homepage')
    @footer = @all_pages.find_by_layout('footer')
    # @structural_sections = @all_pages.roots.find_by_layout('structure_page')&.descendants
    @privacy_sitemap = @all_pages.find_by_layout('privacy')
    @cookies_sitemap = @all_pages.find_by_layout('cookie')
    @root_categories = @current_ecommerce.categories.roots.includes(:children)
    @all_categories = @current_ecommerce.categories.order(lft: :asc)
    @all_product_ids = @current_ecommerce.products.pluck(:id)
  end

  #
  def detect_browser
    # for more info on the browser gem see => https://github.com/fnando/browser
    @browser = Browser.new(request.user_agent)
  end

  # sets if a mobile device requested the desktop version
  # this should be used as a before filter and BEFORE any other filter that performs any form of tracking, analysis, counter and so on
  # because this method performs a redirect to the same url without the dsktp_v parameter, this implies that any filter before this one will be called twice
  def mobile_viewport
    # if the url contains the parameter params[:dsktp_v] == "1", activate the desktop version on mobile, if is == "0" return to the mobile version
    if ['1', '0'].include?(params[:dsktp_v])
      cookies[:dsktp_v] = params[:dsktp_v] # sets the cookie
      redirect_to url_for(params.except(:dsktp_v).merge(only_path: true)) # redirect to the same url without the dsktp_v parameter
    end

    @force_desktop_version = cookies[:dsktp_v] == '1'
  end

  # the account is assigned to a dynamic variable, so that it can be easily overridden by our staff to simulate another user
  def set_account
    @current_account = current_account
  end

  # set the cart
  def set_cart
    cart_id = session["#{ecommerce_name}_cart_id"]

    # the default scope will get rid of selecting only the carts for the appropriate ecommerces
    if @current_account.present?
      @current_cart = @current_account.shopping_carts.in_progress.last

      # merge the cart with the cart saved in the session (this happens when the user adds items to the cart and then logins, but he has a cart associated to its account)
      # so what we do here is add every product of the new cart to the cart associated to its account and then destroy the anonymous cart
      anonymous_cart = ShoppingCart.anonymous.in_progress.find_by_token(cart_id) if cart_id.present?

      if anonymous_cart.present? && @current_cart.present?
        # ATTENTION
        # the cart merging process will recalculate the price of the items with the price they have now, while this behavior is good in almost every case
        # it can have repercussions with the prices increasing overtime:
        # If I add the product, then login 10 mins later, the price will be updated and the product will cost more (this is a good thing if the promotion has ended in the 10m span)
        # (the prices over time will probably need their own processing logic as they'll probably need to be purchased in a small time window)
        @current_cart.merge_with_cart(anonymous_cart)
        anonymous_cart.destroy
      elsif anonymous_cart.present?
        @current_cart = anonymous_cart
        @current_cart.customer = @current_account
        @current_cart.user_ip = request.remote_ip
        @current_cart.save
      end
    else
      @current_cart = ShoppingCart.anonymous.in_progress.find_by_token(cart_id)
    end

    @current_cart ||= ShoppingCart.new({ ecommerce: @current_ecommerce, customer: @current_account })

    # if the user changes the ecommerce language, the id of the current_ecommerce is different
    # we need to reinitialize the cart with the same items and associate it to the new ecommerce
    # the old cart's items will be re-added one by one to the cart (that's the correct behaviour) because
    # a product can be unavailable or can have a different price in the new ecommerce and those factors will be considered when merging the carts
    if @current_cart.ecommerce != @current_ecommerce
      old_cart = @current_cart
      @current_cart = ShoppingCart.new({ecommerce: @current_ecommerce, customer: @current_account})
      old_cart.destroy if @current_cart.merge_with_cart(old_cart)
    end

    # we just set those attributes here WITHOUT SAVING THE CART for the following reasons:
    # 1 - we don't want a blocking UPDATE on the database performed on each request
    # 2 - those informations are stored in a the session and are always available for each subsequent request, we won't lose them
    # 3 - if the user performs a significant action (such as adding a product, purchasing the cart...), the cart will be saved along with those attributes
    @current_cart.browser_info = request.user_agent
    @current_cart.user_ip = request.remote_ip
    @current_cart.referer_external = @tracked_parameters[:referer_external]
    @current_cart.promo_code = @tracked_parameters[:lsh]
    @current_cart.locale = I18n.locale

    session["#{ecommerce_name}_cart_id"] = @current_cart.token
    @current_cart_size = @current_cart.products.size
  end

  # returns the parameterized ecommerce name from the config.yml file
  def ecommerce_name
    ENV['NAME'].parameterize.underscore
  end

  # override the devise helper to store the current location so we can
  # redirect to it after loggin in or out. This override makes signing in
  # and signing up work automatically.
  def store_current_location
    store_location_for(:account, request.url)
  end

  # enables the vacation message for the whole site
  def set_vacation_message
    @render_vacation_message = true if ENV['VACATION_ALERT_SITE']
  end

  #
  def set_tracking_services
    @trackings = ::Tracking.for_ecommerce(@current_ecommerce)
  end
end
