////////////////////////////////////////////////////////
// address change current class in confirm step
////////////////////////////////////////////////////////

$(function(){
  var $addressesCont = $(".shipping-address .addresses");
  var $addresses = $addressesCont.find(".select-address");
  var $changeShipping = $(".shipping-address .change-shipping");
  var $undoChangeShipping = $(".shipping-address .undo-change-shipping");
  var $form = $(".cart-confirm form#cart-form");

  // on change address
  $addresses.find("input[type=\"radio\"]").change(function(e){
    $addresses.removeClass("current");
    $(this).parents(".select-address").first().addClass("current");
    $form.submit();
  })

  // sets the initial selected
  $addresses.find("input[type=\"radio\"]:checked").parents(".select-address").first().addClass("current");


  // toggle the addresses visibility
  $changeShipping.click(function(e){
    e.preventDefault();
    $(this).hide();
    $addressesCont.addClass("visible");
  })

  $undoChangeShipping.click(function(e){
    e.preventDefault();
    $changeShipping.show();
    $addressesCont.removeClass("visible");
  })
})