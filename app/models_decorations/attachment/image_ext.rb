#
module Attachment::ImageExt
  extend ActiveSupport::Concern

  # for documentation on image sizing please check lipsiastore4 project on following file:
  # app/ecommerces/models/attachment/image.rb, class method url

  # overrides paperclip assets relative url in development
  # changes from /uploads/image.jpg to /assets/image.jpg, this is necessary because on development, the lipsiastore4 images are automatically downloaded from the production server
  # and passed as assets to the ecommerce application
  included do
    if Rails.env.development?
      alias_method :old_url, :url

      def url(*args)
        old_url(*args).gsub(/^(\/uploads|\/thumbs)/, Rails.configuration.assets.prefix)
      end
    end
  end
end
