#
class Accounts::ShippingAddressesController < AccountsController
  before_action :set_account_action
  add_breadcrumb I18n.t('shipping_addresses'), :accounts_shipping_addresses_path

  #
  def index
    @addresses = @current_account.shipping_addresses
  end

  #
  def new
    add_breadcrumb t('new_shipping_address')
    @address = @current_account.shipping_addresses.new
    render 'form'
  end

  #
  def create
    @address = @current_account.shipping_addresses.new(address_params)
    if @address.save
      flash[:shipping_address_success] = t('shipping_address_saved')
      redirect_to edit_accounts_shipping_address_path(id: @address)
    else
      add_breadcrumb t('new_shipping_address')
      render 'form'
    end
  end

  #
  def edit
    @address = @current_account.shipping_addresses.find(params[:id])
    add_breadcrumb t('edit_shipping_address'), edit_accounts_shipping_address_path(id: @address)
    render 'form'
  end

  #
  def update
    @address = @current_account.shipping_addresses.find(params[:id])
    flash.now[:shipping_address_success] = t('shipping_address_saved') if @address.update(address_params)

    render 'form'
  end

  #
  def set_as_default
    @address = @current_account.shipping_addresses.find(params[:id])
    @address.set_as_default!

    redirect_to action: :index
  end

  #
  def destroy
    @address = @current_account.shipping_addresses.find(params[:id])
    @address.destroy
    redirect_to action: :index
  end

  private

  #
  def set_account_action
    @account_action = 'shipping_addresses'
  end

  #
  def address_params
    params.require(:shipping_address).permit(
      %i[name address address_2 country province city cap telephone note
         telephone_prefix]
    )
  end
end
