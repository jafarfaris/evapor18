#
class Frontend::CartController < FrontendController
  # list of controller actions accessible by everyone without restrictions
  public_actions = %i[add_product remove_item add_products_from_url update show recap]

  before_action :authenticate_account!, except: public_actions
  before_action :cart_is_not_empty!, except: public_actions
  before_action :set_cart_vacation, :set_add_on_products

  add_breadcrumb I18n.t('my_cart'), :cart_show_path

  #############################################
  # Cart manipulation actions
  #############################################

  # add a product
  def add_product
    @product = Product.unscope(:where).find(params[:product_id])
    @quantity = params[:quantity].to_i
    redirect_url = params[:redirect_url]

    @success = @current_cart.add_product(product: @product, quantity: @quantity)

    respond_to do |format|
      format.html do
        redirect_to(redirect_url.present? ? redirect_url : { action: :show })
      end

      format.js { render layout: false }
    end
  end

  # remove a product
  # this actions accepts the ShoppingCart::Item id as an argument
  def remove_item
    @item = @current_cart.items.find(params[:item_id])
    @current_cart.remove_item(@item)

    respond_to do |format|
      format.js { render layout: false }
    end
  end

  # this action will add the products from a static GET url
  # for internal use, the "add_product" action should be used instead.
  # this action's purpose is to create carts from external urls coming from
  # newsletter, promotions and so on...
  # it accepts the following parameters:
  # product_ids   => a series of comma separated ids  (1234,3452,214)
  # qt            => a series of quantities comma splitted. they should be the
  #                  same length as the ids, the first quantity will be
  #                  associated with the first id and so on...
  # reset         => if reset equals "1" the current cart will be wiped and the
  #                  products will be added in a new fresh cart
  # cpc           => coupon code
  # example url: /it/add_products/12654,12359&qt=1,1&reset=1
  def add_products_from_url
    products_ids = params[:product_ids].split(',').map(&:to_i) # products_ids
    quantities = params[:qt].split(',').map(&:to_i) # quantities
    coupon_code = params[:cpc]
    # if this parameter is set equal to 1, the cart will be reinitialized losing
    # all its current content
    reset_cart = params[:reset] == '1'
    redirect_url = params[:redirect_url]

    # ensure that the quantities and the ids corresponds
    if products_ids.size == quantities.size
      if reset_cart
        @current_cart.archive!
        set_cart # initializes a new shopping cart
      end

      products_ids.each_with_index do |prod_id, i|
        product = Product.find(prod_id)
        @current_cart.add_product(product: product, quantity: quantities[i].to_i)
      end

      @current_cart.update({ coupon_code: coupon_code }) if coupon_code.present?
    end

    redirect_to(redirect_url.present? ? redirect_url : { action: :show })
  end

  # this action is called in the show cart and it receives the cart form with the items, their quantity and the coupon
  # it will then redirect either to the cart show action or the address action based on the submit button clicked
  def update
    # we could update every product easilly with
    # cart_params = params.require(:shopping_cart).permit(items_attributes: [:id, :quantity])
    # @current_cart.update(cart_params)
    # BUT! it will fail to update every item if only one of them is invalid and it won't display the items error messages for some reason...
    # so we iterate through each item manually and update them one by one

    @items = []

    params.require(:shopping_cart).require(:items_attributes).each_pair do |index, pars|
      item = @current_cart.items.find( pars[:id] ) rescue next
      @items << item # we need to keep the item in the @items array or we won't be able to display the validation errors in the view
      item.update(quantity: pars[:quantity])
    end

    # TODO: need further check on this failsafe cond
    # if !@current_cart.empty? # failsafe condition, if the user has two tabs, one on the show action, another on the purchase and he purchases the cart on the second tab
                             # he then has the show tab with the previous cart on the show action, but that cart is no longer valid, here we ensure that the current_cart
                             # must be filled with some products, otherwise, we won't update it

    # go back to the cart show action, /!\ don't do a redirect, or you'll lose the item warnings and error messages (without redirect there's the drawback that the url changes)
    @payments = @current_ecommerce.payments.publics.ordered
    @deliveries = @current_cart.possible_deliveries

    if params[:after_update] == 'account'
      @success = @current_cart.update(params.require(:shopping_cart).permit(:payment_id).merge({ validate_payment: true }))
      # continue with the login/register if the checkout button is pressed and the cart has been successfully updated
      # redirect_to cart_registrations_login_or_register_path if @success
      # respond_to do |format|
      #   # format.html { redirect_to cart_registrations_login_or_register_path if success }
        # format.js { redirect_to cart_registrations_login_or_register_path if @success }
      #   format.js { render layout: false }
      # end
    else
      @current_cart.update( params.require(:shopping_cart).permit(:payment_id) )
    end

    respond_to do |format|
      # format.html { render :show }
      format.js { render layout: false }
    end
    # end
  end


  #############################################
  # END Cart manipulation actions
  #############################################

  #############################################
  # Cart steps actions
  #############################################

  def show
    @current_cart.set_progress!(ShoppingCart::Progresses.visualization)
    @items = @current_cart.items.where.not(type: 'ShoppingCart::AddOn')
    @payments = @current_ecommerce.payments.publics.ordered
    # @deliveries = @current_cart.possible_deliveries
  end

  # the account login and register are in the Frontend::Cart::RegistrationsController

  # if the account is a basic account or doesn't return true on the can_order? method, it means that he needs to fill some more details
  def missing_details
  end

  # this action will receive the form coming from the missing_details's view
  def fill_details
    account_params = params.require(:account).permit([
      :cfis, :address, :address_2, :country, :province, :city, :cap,
      :telephone, :phone, :telephone_prefix, :phone_prefix,
      :birthday, :sex
    ])
    # promotes the account to cliente if its a basic user
    account_params.merge!({role: "cliente"}) if @current_account.role == "basic_user"
    if @current_account.update(account_params)
      redirect_to action: :confirm
    else
      render "missing_details"
    end
  end




  # changes the cart's shipping address
  def change_shipping
    add_breadcrumb t("shipping")
    @current_cart.set_progress!(ShoppingCart::Progresses.shipping)
    # redirect if there's a default shipping address, the user will be able to change it in the last step
    @address = @current_account.shipping_addresses.build
  end

  # saves a new shipping address (change_shipping should POST to this action)
  def update_shipping
    @address = @current_account.shipping_addresses.build(address_params)

    if @address.save
      @current_cart.update(shipping_address: @address)
      redirect_to action: :confirm
    else
      render "change_shipping"
    end
  end

  # final step, the user can review and edit the order details, then conclude the order
  def confirm
    @current_cart.validate_payment = true
    return redirect_to action: :show unless @current_cart.valid?
    @current_cart.set_progress!(ShoppingCart::Progresses.confirmation)
    # it's the final step before making the order
  end

  # step after the confirm, this will actually generate the order
  def make_order
    return redirect_to action: :confirm if request.method == "GET"

    # the order is generated only if the after_update param is equal to "make_order"
    if params[:after_update] == "make_order"
      # TODO uncomment the code below and remove the render "order_completed" on the last line

      if @current_cart.purchase!
        # resets all the tracked stats
        session["_#{ecommerce_name}_trk"] = nil
        @tracked_parameters = HashWithIndifferentAccess.new
        @skip_unpaid_orders = true

        return render "order_completed"
      else
        return render "confirm"
      end
    else
      # otherwise, the cart will get updated and the confirm page will be re-rendered
      @current_cart.update( params.require(:shopping_cart).permit(:shipping_address_id, :coupon_code).merge({validate_payment: true}) )
      return render "confirm"
    end
  end

  protected

  # ensures that the cart has at least an item, otherwise the user gets redirected to the cart-show
  def cart_is_not_empty!
    redirect_to action: :show if @current_cart.empty?
  end

  #
  def address_params
    params.require(:shipping_address).permit([:name, :address, :address_2, :country, :province, :city, :cap, :telephone, :telephone_prefix, :note])
  end

  #
  def set_cart_vacation
    @render_vacation_message = true if ENV['VACATION_ALERT_CART']
  end

  #
  def set_add_on_products
    @add_on_products = Product.add_ons
  end
end
