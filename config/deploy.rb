# config valid only for current version of Capistrano
lock '3.4.0'

Dotenv.load(".env.#{fetch(:stage)}")

set :application, ENV['NAME']
set :repo_url, "git@github.com:LipsiaGROUP/#{fetch(:application)}.git"


# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name


set :ssh_options, { forward_agent: true }

# Default value for :scm is :git
# set :scm, :git
set :deploy_via, :remote_cache
set :keep_releases, 2
set :normalize_asset_timestamps, false

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
set :pty, false

# Default value for :linked_files is []
set :linked_files, fetch(:linked_files, []).push(".env.#{fetch(:stage)}")

# Default value for linked_dirs is []
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'tmp/exports', 'public/system', 'public/uploads', 'public/public_uploads', 'public/thumbs')



namespace :deploy do
  namespace :passenger do
    task :restart do
      on roles(:web) do
        begin
          execute "passenger-config restart-app #{current_path}"
        rescue
          begin
            execute "rvmsudo passenger-config restart-app #{current_path}"
          rescue
            execute :touch, "#{current_path}/tmp/restart.txt"
          end
        end
      end
    end
  end


  desc "uploads the local .env file to the server"
  task :upload_env_file do
    on roles(:app) do
      env_name = ".env.#{fetch(:stage)}"
      env_file = File.expand_path("../../#{env_name}", __FILE__)
      divider = "-"*50
      puts divider

      if File.exist?( env_file )
        puts "Uploading file #{env_name}"
        upload! env_file.to_s, "#{shared_path}/#{env_name}"
        puts "success"
      else
        puts "The file #{env_name} doens't exists on your local machine and it won't be uploaded"
      end

      puts divider
    end
  end
end
