#
Rails.application.routes.draw do
  # redirects to the appropriate lang
  get '', to: 'frontend#redirect_lang'

  # Start localized routes
  scope '/:locale', locale: /#{Ecommerce.find(ENV['ECOMMERCE_ID']).locales.join('|')}/ do
    # devise
    devise_for :accounts, skip: :registrations, controllers: {
      sessions: 'accounts/sessions',
      passwords: 'accounts/passwords'
    }, path_names: {
      sign_in: 'login', sign_out: 'logout',
      password: 'password'
      # , confirmation: 'verification',
      # registration: 'register', edit: 'edit'
    }
    devise_scope :account do
      # devise registration
      resource :account_registration, only: [:new], path: "accounts", path_names: {new: "register"}, controller: 'accounts/registrations'
      post "/accounts", controller: "accounts/registrations", action: :create_base, as: :account_registration
      get "/accounts/register/sign_up_vat", controller: "accounts/registrations", action: :new_vat, as: :new_vat_account_registration
      post "/accounts/register_vat", controller: "accounts/registrations", action: :create_vat, as: :vat_account_registration

      resource :account, only: [:edit, :update] do
        get :edit_vat
        get :edit_password
        match :update_vat, via: [:put, :patch]
        match :update_password, via: [:put, :patch]
      end
      # get "/accounts/edit", controller: "accounts", action: :edit, as: :edit_account
      # match "/accounts", controller: "accounts", action: :update, as: :account, via: [:put, :patch]
    end

    # additional devise custom routes
    # namespace :accounts, as: "account", module: nil do
    #   namespace :register, as: "registration", module: "accounts/registrations" do
    #     # get "/accounts/register/sign_up_vat", action: :new_vat, as: :new_vat_account_registration
    #     resource :billing_address, only: [:new, :edit, :update]
    #   end
    # end

    # end devise


    namespace :accounts do
      get :dashboard

      resources :orders, only: [:index, :show] do
        member do
          match :cancel, via: [:put, :patch]
          get :payment
          post :payment, action: :update_payment, as: "update_payment"
        end
      end

      resources :shipping_addresses, except: [:show] do
        match :set_as_default, on: :member, via: [:patch, :put]
      end
    end


    scope module: 'frontend' do
      root to: 'pages#index'
      resources :pages, only: [:show]

      resources :categories, only: [:index, :show]
      resources :products, only: [:show] do
        collection do
          get :price_calc
        end
      end

      # cities and provinces routes
      namespace :addresses do
        # get ":country_id/provinces", action: "provinces", as: :provinces
        get ":province_name/cities", action: "cities", as: :cities
        get ":province_name/:city_name/suggest-cap", action: "suggest_cap", as: :suggest_cap
      end

      namespace :cart do
        root action: :show, as: :show
        post 'add_product/:product_id', action: :add_product, as: :add_product
        delete 'remove_item/:item_id', action: :remove_item, as: :remove_item
        # adds more products via get url
        get 'add_products/:product_ids', action: :add_products_from_url, as: :add_products
        # updates the cart quantities and starts the checkout process
        match '', action: :update, as: :update, via: [:patch, :put]

        # devise registraion and login from the cart
        devise_scope :account do
          namespace :sessions do
            post :create
          end
          namespace :registrations do
            get  :login_or_register
            get  :login_or_register_vat
            post :create_customer
            post :create_vat_account
          end
        end

        # ship address
        get :missing_details
        match :fill_details, via: [:put, :patch]

        get :change_shipping
        post :update_shipping

        get :confirm
        match :make_order, via: [:get, :post]  # final update action that will update the shipping/payment or generate the order
                                               # the GET method is mapped too to correctly intercept the "go back" buttons, a GET request shoul be redirected to the :confirm action
      end

      # path for posting contact forms
      namespace :contacts do
        # post :standard
        post :dynamic, :newsletter
      end

      scope module: "payments" do
        # /en/paga/81/ORD-4973/91.21
        get "paga/:payment_id/:custom_id/:total", action: :external_payment, constraints: {total: /\d+\.\d{2}/}
      end

    end

    # localized error pages
    get '*path', to: 'exceptions#not_found'
  end
  # end localized routes


  # non localized error pages
  get '*path', to: 'exceptions#not_found'
  # Rails expects the error pages to be served from /<error code>.
  match '400', to: 'exceptions#not_found', via: :all
  # error 500 is managed by the file in the public folder, a server error should render a static page!

end
