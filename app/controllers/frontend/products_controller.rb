#
class Frontend::ProductsController < FrontendController
  include ProductViewsHistory
  before_action :set_product_vacation

  #
  def show
    @product = Product.find(params[:id])

    # redirect if url only contains id, before we do too much process
    redirect_to product_path(id: @product) if @product.to_param != params[:id]

    @alt_products = Product.where.not(id: @product.id)
    @categories = @product.categories.where(ecommerce_id: @current_ecommerce.id).active

    # we get options from sitemap
    @sitemap = @all_pages.find_by layout: 'product_detail'

    defaults = "{ product_thumbnail_direction: 'vertical', use_sidebar: 'false', alternative_versions: 'dropdown', add_to_cart_effect: 'tooltip' }"
    @options = ApplicationController.helpers.get_layout_options(@sitemap, YAML.safe_load(defaults))

    # breadcrumbs
    add_breadcrumb(t('products.products'), categories_path)

    if @categories.present?
      # if a product belongs to many categories, we'll only take the first one
      # and its ancestors
      @first_category = @categories.order(lft: :desc).first

      @first_category.self_and_ancestors.order(lft: :asc).each do |category|
        add_breadcrumb(category.text, category_path(category))
      end
    end

    add_breadcrumb(@product.name, product_path(@product))

    # save product history to cookies & DB
    track!
  end

  #
  def price_calc
    @product = Product.find(params[:product_id])

    # minimum order amount that eligible for free shipping
    free_shipping = @current_ecommerce.minimal_order_amount_for_free_delivery

    # total order that already in cart
    total_cart_price = @current_cart.total_with_vat

    # total price for current product (the one getting calculated right now)
    @total_product_price = (params[:quantity].to_i * @product.price_with_vat)

    # total price is taken from both total in cart and current product that are
    # not in cart yet
    total_price = (total_cart_price + @total_product_price)

    @shipping_price =
      if !free_shipping.zero? && total_price >= free_shipping
        # free shipping enabled & total price > minimum purchase for free shipping
        0
      else
        # free shipping disabled
        @product.extra_ship_price
      end
    @total_price = @total_product_price + @shipping_price

    respond_to do |f|
      f.js { render layout: false }
    end
  end

  protected

  #
  def set_product_vacation
    if !request.xhr? && ENV['VACATION_ALERT_PRODUCT']
      @render_vacation_message = true
    end
  end
end
