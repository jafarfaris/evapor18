// to make the inputs targeted by this script you can either:
// - assign the class "floating-labels" to a parent element (usually directly to the form tag)
// - assign the class "floating-label" to the parent element of a single input


(function($){
  // editable vars
  var excludeInputsType = ["checkbox", "radio", "submit", "hidden", "date"];

  var wrapperClass = "floating-label-wrapper";
  var floatingClass = "floating-visible";
  var focussedClass = "focussed";
  // end editable variables

  // Input class
  function Input($el){
    var input = this;
    input.$el = $el;
    input.$label = findLabel($el);
    input.$wrapper = createWrapper( input.$el );


    input.focus = function(){
      input.$wrapper.addClass( focussedClass );
    };

    input.unfocus = function(){
      input.$wrapper.removeClass( focussedClass );
    };

    input.updateLabel = function(){
      if ( input.$el.val() ){
        input.$wrapper.addClass( floatingClass );
      } else {
        input.$wrapper.removeClass( floatingClass );
      }
    }

    // updates the input classes (used to set the initial classes on dom ready)
    input.setStatus = function(){
      if ( input.$el.is(":focus") ){
        input.focus();
      } else {
        input.unfocus();
      }

      if ( input.$el.val() ){
        // makes label appear without animation
        input.$label.css({transition: "none 0s ease 0s"});
        input.$wrapper.addClass( floatingClass );
        input.$label.css("opacity"); // force element redraw
        input.$label.css({transition: ""}); // remove the transition inline style
      } else {
        input.$wrapper.removeClass( floatingClass );
      }

    };


    // private functions

    // find the closest label for the input element passed as argument
    function findLabel($input){
      var $label = $input.data("label");

      // search for the label if it's the first time that the input is changing
      if ( $label === undefined ){
        var id = $input.attr("id");

        // 1 - try to find the closest label
        var $label = $input.siblings("label");
        // 2 - try to see if the label is the parent
        $label = ( $label.length > 0 ) ? $label : $input.closest("label");
        // 3 - search the label in the whole document by the attribute for
        var $labelByFor = $(document).find("label[for=\"" + id + "\"]");
        $label = ( $label.length > 0 ) ? $label : $labelByFor;

        $input.data("label", $label);
      }

      return $label;
    };

    // create a wrapper around the input and its label
    function createWrapper($input){
      var $wrapper = $("<div/>").addClass(wrapperClass);
      input.$el.add(input.$label).wrapAll( $wrapper );
      return input.$el.parent("." + wrapperClass);
    };

    
    function init(){
      // auto binds inputs change
      input.$el.on("propertychange change click keyup input paste", input.updateLabel);

      // if the input is focussed, color the label
      input.$el.on("focus", input.focus);
      input.$el.on("blur", input.unfocus);

      input.setStatus();
    }

    init();

    return input;
  }



  // jQuery plugin
  $.fn.floatingLabels = function(){
    this.each(function(){
      var $this = $(this);
      var type = $this.attr("type");

      if ( $this.is("input") && excludeInputsType.indexOf(type) !== -1 )
        return true; // skip to the next iteration

      if ($this.data('floatingLabel') === undefined){
        $this.data("floatingLabel", new Input($this) );
      }
    })

    return this;        // return jQuery selector
  };

})(jQuery);
