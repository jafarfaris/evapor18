#
module CategoryHelper
  #
  def parse_nested_categories(categories, &block)
    return '' if categories.size == 0

    # ul specified directly in view, not here (some not use it like main nav)
    output = '<li>'
    path = [nil]

    categories.each_with_index do |o, i|
      if o.parent_id != path.last
        # We are on a new level, did we descend or ascend?
        if path.include?(o.parent_id)
          # Remove the wrong trailing path elements
          while path.last != o.parent_id
            path.pop
            output << '</li></ul>'
          end
          output << '</li><li>'
        else
          path << o.parent_id
          output << '<ul><li>'
        end
      elsif i != 0
        output << '</li><li>'
      end
      output << capture(o, path.size - 1, &block)
    end

    output << '</li>' * path.length
    output.html_safe
  end
end
