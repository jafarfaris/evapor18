#
module PaymentExt
  extend ActiveSupport::Concern

  included do
    default_scope -> { joins(:ecommerces).where(ecommerces: { id: ENV['ECOMMERCE_ID'] }).distinct }
  end
end
