#
class Accounts::RegistrationsController < Devise::RegistrationsController
  include AccountRegistrable
  add_breadcrumb I18n.t('register')

  # this action adds additional custom parameter and then calls the devise's
  # default create
  def create_base
    @additional_resource_params = {
      role: 'basic_user',
      ecommerce_id: @current_ecommerce.id,
      parent_id: EcommerceConfig.store_id
    }
    @failure_view = 'new'
    configure_permitted_parameters
    create_account
  end

  # #
  # def new_vat
  #   build_resource({})
  #   yield resource if block_given?
  #   respond_with resource
  # end

  # #
  # def create_vat
  #   @additional_resource_params = {
  #     role: 'vat_customer',
  #     ecommerce_id: @current_ecommerce.id,
  #     parent_id: EcommerceConfig.store_id
  #   }
  #   @failure_view = 'new_vat'
  #   configure_permitted_parameters
  #   create_account
  # end
end
