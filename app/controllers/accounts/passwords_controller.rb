#
class Accounts::PasswordsController < Devise::PasswordsController
  add_breadcrumb I18n.t('retrieve_password')

  # GET /resource/password/new
  # def new
  #   super
  # end

  # POST /resource/password
  # def create
  #   super
  # end

  # GET /resource/password/edit?reset_password_token=abcdef
  # def edit
  #   super
  # end

  # PUT /resource/password
  def update
    # devise doesn't check if the password is blank...
    return redirect_to :back if resource_params[:password].blank?
    super
  end

  protected

  #
  def after_resetting_password_path_for(resource)
    super(resource)
  end

  # The path used after sending reset password instructions
  # def after_sending_reset_password_instructions_path_for(resource_name)
  #   super(resource_name)
  # end
end
