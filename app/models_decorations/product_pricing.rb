# some method in this concern should be moved to lipsiastore4 after we make sure
# everything works well with other ecommerces
#
module ProductPricing
  # extend ActiveSupport::Concern

  # included do
  #
  def has_reseller_price?
    !$_ACCOUNT.nil? && (account = Account.find($_ACCOUNT)).reseller? && account.price_lists.where(product_id: id).present?
  end

  #
  def reseller_price
    # fallback if not reseller
    price
    return Account.find($_ACCOUNT).price_lists.find_by_product_id(id).price if has_reseller_price?
  end

  #
  def has_local_price?
    international_prices.present? && !has_reseller_price?
  end

  #
  def price_table
    # price_table will be products, except if international_prices present
    international_prices.present? ? international_prices.first : self
  end

  # if have valid international price, this will check if it has discount.
  # otherwise this will check if product has discount directly
  # example cases:
  # 1. empty start date & end date > now will be consider as VALID discount
  # 2. start date <= now & empty end date also VALID discount
  # 3. empty start date & end date, also VALID
  # 4. start date < now & end date > now: VALID
  # 5. other than above will be considered as INVALID discount
  def has_discount?
    if has_reseller_price?
      false
    else
      (!price_table.price_list.to_f.zero? && price_table.price_list > price_table.price) && (!price_table.discount_from.nil? && !price_table.discount_to.nil? ? ((price_table.discount_from.nil? || price_table.discount_from <= Time.zone.now) && (price_table.discount_to.nil? || price_table.discount_to >= Time.zone.now)) : true)
    end
  end

  #
  def discount_percentage
    0
    # don't use price_with_vat
    return ((price_table.price_list - price_table.price) / price_table.price_list * 100).round if !has_reseller_price? && has_discount?
  end

  # this method exists because in the future the "price" field will be without
  # vat and we will introduce dynamic prices
  def price_with_vat
    has_reseller_price? ? reseller_price : price_table.price
  end

  # priority:
  # 1. account's custom VAT
  # 2. local product's VAT (set personally on international price)
  # 3. product's VAT
  def applied_vat
    base_vat = (custom_vat = price_table.vat.to_f).zero? ? vat : custom_vat

    if $_ACCOUNT.present? && (account = Account.find($_ACCOUNT)).present?
      !account.custom_vat.to_f.zero? ? account.custom_vat : base_vat
    else
      base_vat
    end
  end

  # Returns product price without VAT
  #   e.g. VAT 22% => price/1.22
  #        VAT 21% => price/1.21
  #        VAT 10% => price/1.10
  #        VAT 4%  => price/1.04
  def price_without_vat
    price_with_vat / (applied_vat.to_f / 100.0 + 1)
  end
  # end
end
