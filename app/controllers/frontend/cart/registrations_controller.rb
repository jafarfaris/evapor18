#
class Frontend::Cart::RegistrationsController < Devise::RegistrationsController
  add_breadcrumb I18n.t('my_cart'), :cart_show_path
  add_breadcrumb I18n.t('login_or_register')
  include AccountRegistrable

  before_action :set_cart_progress, :proceed_if_logged_in

  # first step of the checkout
  def login_or_register
    build_resource({})
    yield resource if block_given?
    respond_with resource
  end

  # first step of the checkout for vat accounts registration
  def login_or_register_vat
    build_resource({})
    yield resource if block_given?
    respond_with resource
  end

  # creates the account of a "cliente"
  def create_customer
    @additional_resource_params = {
      role: "cliente",
      ecommerce_id: @current_ecommerce.id,
      parent_id: EcommerceConfig.store_id
    }
    @failure_view = "login_or_register"
    configure_permitted_parameters
    create_account
  end

  # creates the account of a "vat_customer"
  def create_vat_account
    @additional_resource_params = {
      role: "vat_customer",
      ecommerce_id: @current_ecommerce.id,
      parent_id: EcommerceConfig.store_id
    }
    @failure_view = "login_or_register_vat"
    configure_permitted_parameters
    create_account
  end

  private

  # updates the cart progress
  def set_cart_progress
    @current_cart.set_progress!(ShoppingCart::Progresses.registration)
  end

  # if the user is logged in and can purchase there's no reason to show anything of this controller
  def proceed_if_logged_in
    redirect_to next_cart_step if @current_account.present?
  end

  protected

  #
  def after_sign_up_path_for(resource)
    next_cart_step
  end

  #
  def next_cart_step
    @current_account.can_order? ? cart_confirm_path : cart_missing_details_path
  end
end
