#
class Frontend::ContactsController < FrontendController
  #
  def dynamic
    session[:contact_standard] = params[:contact].permit(:email, :name, :message, :telephone, :privacy).merge({ecommerce_id: @current_ecommerce.id, typology: Contact.typologies["Contatto Generico"], remote_addr: request.remote_ip })
    @contact = Contact.new(session[:contact_standard])
    session[:dynamic_contact] = params[:contact].permit(
      :name, :surname, :telephone, :phone, :company, :address, :city, :province,
      :cap, :country, :email, :message, :budget, :region, :telephone_prefix,
      :phone_prefix, :birth_date, :form_id, :typology, :privacy
    ).merge({
      ecommerce_id: @current_ecommerce.id,
      remote_addr: request.remote_ip
    })
    @contact = Contact.new(session[:dynamic_contact])

    if @contact.save
      session[:dynamic_contact] = nil
      flash[:success] = I18n.t('contacts.success')
      render layout: false
      NotifierMailer.mail_info(@contact).deliver_now
    else
      flash[:error] = @contact.errors.full_messages
      render layout: false
    end
  end

  #
  def newsletter
    session[:contact_newsletter] = params[:contact].permit(:email, :name, :surname, :privacy).merge({
      ecommerce_id: @sitemap_ecommerce.id,
      typology: Contact.typologies['Newsletter'],
      remote_addr: request.remote_ip })
    @contact = Contact.new(session[:contact_newsletter])

    respond_to do |f|
      if @contact.save
        f.js do
          session[:contact_newsletter] = nil
          flash.now[:success] = I18n.t("contacts.newsletter.success")
          render layout: false
          NotifierMailer.mail_info(@contact).deliver_now
        end
      else
        f.js do
          flash.now[:error] = @contact.errors.full_messages
          render layout: false
        end
      end
    end
  end
end
