require 'active_support/concern'

module OgImageable
  extend ActiveSupport::Concern

  included do
    
    def og_image
      imgs = self.images.og_images
      imgs = self.images.normals unless imgs.present?
      imgs.present? ? imgs.first : nil
    end

  end

end