function cfisCalc(options){

  var defaults = {
    container: ".cfis-generator",
    button: ".fiscal-code-gen",
    errors: ".cfis-errors",
    input: "input.cfis-input",
    translations: {
      error: "The following fields are invalid:",
      name: "Name",
      lastname: "Surname",
      isMale: "Sex",
      day: "Day",
      month: "Month",
      year: "Year",
      communeName: "City"
    },
    data: {}
  };

  var options = $.extend(true, defaults, options);

  var $button = $(options.button);
  var $container = $(options.container);
  var $errors = $(options.errors);

  var $form = $button.parents("form");
  var $country = $form.find("input[name=\"account[country]\"], select[name=\"account[country]\"]");

  // those fields will be displayed if the used needs to fill a missing data for the cfis generation
  var cfisFields = {
    name: $container.find(".cfis-name"),
    lastname: $container.find(".cfis-lastname"),
    day: $container.find(".cfis-birth_date"),
    month: $container.find(".cfis-birth_date"),
    year: $container.find(".cfis-birth_date"),
    sex: $container.find(".cfis-sex"),
    communeName: $container.find(".cfis-city")
  }

  // hide them all by default
  for (var k in cfisFields){
    var $field = cfisFields[k];
    $field.find("input, select").val(""); // reset the value of the input field
    $field.hide();
  }

  // stores the fileds that are already present in the form
  var formFields = {
    name: $form.find("input[name=\"account[name]\"]"),
    lastname: $form.find("input[name=\"account[surname]\"]"),
    day: $form.find("input[name=\"account[birthday]\"]"),
    sex: $form.find("select[name=\"account[sex]\"]"),
    communeName: ""
  };
  formFields.month = formFields.day;
  formFields.year = formFields.day;


  // those fields are needed, if one of them is missing, the cfis cannot be calculated
  var checklist = [
    "name", "lastname", "day", "month", "year", "sex", "communeName"
  ];

  // the final data will be read from any existing formFields with the fallback on the cfisField
  // so, for any missing formField, we replace it with the corresponding cfisField
  var fieldsToShow = {};
  for (var i = 0; i < checklist.length; i++){
    var fieldName = checklist[i];

    if ( formFields[fieldName].length <= 0 ){
      fieldsToShow[fieldName] = cfisFields[fieldName];
      formFields[fieldName] = cfisFields[fieldName].find("input, select");
    }
  }



  //function to call to calculate the actial fiscal code
  function calcCode(){

    var $cfis = $form.find("input[name*=\"[cfis]\"]");

    var data = {};


    // name
    if ( formFields.name.length )
      data.name = formFields.name.val();


    // lastname
    if ( formFields.lastname.length )
      data.lastname = formFields.lastname.val();


    // day month and year
    if ( formFields.day.length ){
      var date = formFields.day.val().split("/");
      if ( date[0].length == 2 && date[1].length == 2 && date[2].length == 4 ){
        data.day = date[0];
        data.month = date[1];
        data.year = date[2];
      }
    }


    // sex
    // console.log(formFields.sex);
    if ( formFields.sex.length ){
      var sex = formFields.sex.val();

      if ( sex === "M" ){
        data.isMale = true;
      } else if ( sex === "F" ){
        data.isMale = false;
      } else {
        data.isMale = undefined;
      }
    }


    // city
    if ( formFields.communeName.length )
      data.communeName = formFields.communeName.val();

    var missingData = [];

    // console.log(data);

    for (var i = 0; i < checklist.length; i++){
      var attr = checklist[i];

      if ( attr === "sex") // sex is bound to the isMale variable
          attr = "isMale";

      // checks that the value is not undefined or is not an empty string
      if (data[attr] === undefined || data[attr].length <= 0) {
        // fallback to the default data and check again
        data[attr] = options.data[attr];

        if (data[attr] === undefined || data[attr].length <= 0) {
          missingData.push(attr);
        }
      }
    }

    // console.log(data);


    if (missingData.length > 0){
      var errorMessages = [options.translations.error];

      for (var i = 0; i < missingData.length; i++){
        var dataName = missingData[i];

        if ( fieldsToShow[dataName] !== undefined )
          fieldsToShow[dataName].show();
        // add the message only if it's not present
        var message = options.translations[dataName];
        if (errorMessages.indexOf(message) === -1)
          errorMessages.push(message);
      }
      $errors.html(errorMessages.join("<br/>"));
      $errors.show();
    } else {
      $errors.hide();
      $errors.empty();

      var person = new CodiceFiscale(data);
      var code = person.taxCode();

      // console.log(code);
      $(options.input).val(code);
    }
  }


  // if I have the country
  if ( $country.length ){

    // bind the cfis button visibility on the country change
    $country.change(function(){
      if ( $country.val() === "Italy" ){
        // show cfis
        $container.show();
        $button.show();
      } else {
        // hide cfis
        $container.hide();
        $button.hide();
      }
    })

    // sets initial visibility
    $country.trigger("change");
  }



  $button.click(function(e){
    e.preventDefault();

    calcCode();
  });
}
