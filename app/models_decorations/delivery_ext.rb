module DeliveryExt
  extend ActiveSupport::Concern

  included do
    scope :with_country_name, ->(country_name){ where(country: country_name) }
    scope :europeans, ->{ where(country: "Comunità Europea") }
    scope :extra_ce, ->{ where(country: "Extra CE") }
  

  
    def self.for_country(country_name)
      # returns the country specific delivery if present
      dels = with_country_name(country_name)
      return dels if dels.present?
      
      # otherwise, try the european if the country belongs to the EU
      if Country::Eu.include?(country_name)
        return europeans.present? ? europeans : extra_ce
      # Else the extra EU
      else
        return extra_ce
      end
    end

  end

end