#
class ExceptionsController < FrontendController
  protect_from_forgery except: :not_found

  #
  def not_found
    respond_to do |format|
      format.html { render '404', status: 404, layout: 'frontend' }
      format.all { render nothing: true, status: 404, layout: false }
    end
  end
end
