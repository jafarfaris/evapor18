#
module CategoryExt
  extend ActiveSupport::Concern

  included do
    include OgImageable

    default_scope { includes(:ecommerce).active }
  end
end
