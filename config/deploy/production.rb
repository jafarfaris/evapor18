# server-based syntax
# ======================
# Defines a single server with a list of roles and multiple properties.
# You can define all roles on a single server, or split them:

# server 'example.com', user: 'deploy', roles: %w{app db web}, my_property: :my_value
# server 'example.com', user: 'deploy', roles: %w{app web}, other_property: :other_value
# server 'db.example.com', user: 'deploy', roles: %w{db}

# set :deploy_to, "/home/deployer/apps/#{fetch(:application)}"
set :deploy_to, "/home/share/store/#{fetch(:application)}"

# normally should we master
# this is special case for demo
set :branch, :'revamp-core'


# bundler_server  =>  the server that performs the "bundle install" in the project's folder, theorically, it should be only a single server
# sidekiq_server  =>  one or more servers that will run the background jobs
# redis_server    =>  the server used for the cache and the sidekiq queue

server "storage.lipsiaserver.com", user: "kopihub", roles: %w{app}
server "store.lipsiaserver.com", user: "kopihub", roles: %w{web bundler_server sidekiq_server}, no_release: true

set :rvm_roles, %w{web bundler_server sidekiq_server}
set :rvm_ruby_version, '2.3.4'


# bundler settings
set :bundle_roles, :web
set :bundle_servers, -> { roles(fetch(:bundle_roles)) }
set :bundle_without, %w{development test staging}.join(' ')
# set :bundle_binstubs, -> { shared_path.join('~/.rvm/gems/ruby-2.3.1@global/bin/') }


# Defaults to [:web]
# set :assets_roles, [:web, :app]
set :assets_roles, [:bundler_server]
set :assets_prefix, 'assets'

# Defaults to 'assets'
# This should match config.assets.prefix in your rails config/application.rb
#set :assets_prefix, 'prepackaged-assets'

# RAILS_GROUPS env value for the assets:precompile task. Default to nil.
# set :rails_assets_groups, :assets

# If you need to touch public/images, public/javascripts, and public/stylesheets on each deploy
# set :normalize_asset_timestamps, %w{public/images public/javascripts public/stylesheets}

# Defaults to nil (no asset cleanup is performed)
# If you use Rails 4+ and you'd like to clean up old assets after each deploy,
# set this to the number of versions to keep
# set :keep_assets, 1


after "deploy", "deploy:passenger:restart"

# overrides bundler set_bins task, that should fix the bundler path with rvm
# namespace :bundler do
#   task :map_bins do
#     fetch(:bundle_bins).each do |command|
#       bundle_exec = "PATH=\"/path/adjusted/so/it/finds/right/rake:$PATH\"   #{SSHKit.config.command_map[:bundle]} exec "
#       SSHKit.config.command_map.prefix[command.to_sym].clear.push(bundle_exec)
#     end
#   end
# end
