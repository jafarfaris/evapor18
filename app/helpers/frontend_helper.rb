#
module FrontendHelper
  #
  #
  def viewport
    if @force_desktop_version
      'width=1200'
    else
      'width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0'
    end
  end

  # helper to build the title (used in association with the gem metamagic)
  def page_title_for(title, site)
    return site if title.blank?
    title = title.map(&:squish).compact.join(" - ") if title.is_a?(Array)
    title.include?(site) ? title : "#{title} - #{site}"
  end

  # javascript_include_tag and sylesheet_link_tag override to include assets ONLY ONCE
  # this allows to call those methods inside partials without worrying about assets repeating all over the page
  def javascript_include_tag(*sources)
    @included_javascripts ||= []
    sources.reject!{|s| @included_javascripts.include?(s)}
    @included_javascripts += sources
    super(*sources)
  end

  #
  def available_locales
    @current_ecommerce.locales.reject(&:empty?)
  end

  #
  def sylesheet_link_tag(*sources)
    @included_stylesheets ||= []
    sources.reject!{|s| @included_stylesheets.include?(s)}
    @included_stylesheets += sources
    super(*sources)
  end

  #
  def ecommerce_countries
    @_ecommerce_countries ||= Country.all.order("name").pluck(:name)
  end

  #
  def ecommerce_italian_provinces
    @_ecommerce_provinces ||= Province.joins(:country)
      .where(countries: { iso: 'IT' })
      .order('full_description').pluck(:full_description)
  end

  #
  def telephone_prefixes
    @_telephone_prefixes ||= PhonePrefix.order(:prefix).pluck(:prefix).uniq
  end

  #
  def sex_array
    ['M', 'F']
  end

  def account_panel
    content_for :account_content do
      yield
    end

    return render 'accounts/shared/account_panel'
  end


  def cart_panel
    content_for :cart_content do
      yield
    end
    return render partial: "frontend/cart/cart_panel"
  end



  def error_messages_for(record)
    return "" if record.errors.empty?
    # Don't use full_messages because it doesn't skip empty error messages
    messages = record.errors.messages.map do |k, error|
      error.map{|e| content_tag(:li, record.errors.full_message(k, e)) if e.present? }.join
    end.join

    sentence = I18n.t("errors.messages.not_saved",
                      count: record.errors.count,
                      resource: record.class.model_name.human.downcase)

    html = <<-HTML
    <div class="alert alert-danger alert-dismissable" role="alert">
      <button class="close" type="button" data-dismiss="alert" aria-hidden="true">&times;</button>
      <ul>#{messages}</ul>
    </div>
    HTML

    html.html_safe
  end



  def order_status_class(status_text)
    case status_text.parameterize.underscore
    when "annullato", "rimborsato"
      "default"
    when "spedito"
      "success"
    when "parzialmente_spedito", "fatturato", "in_attesa_di_spedizione", "pagato_in_lavorazione"
      "warning"
    when "in_attesa_di_pagamento"
      "danger"
    end
  end

  # take the passed dimension and translate it into a bootrap colum class
  # if not recognized, fallback to full width
  def translate_to_column(dimension)
    klass = case dimension
    when "full"
      klass = "col-sm-12"
    when "big"
      klass = "col-sm-8"
    when "half"
      klass = "col-sm-6"
    when "small"
      klass = "col-sm-4"
    else
      klass = "col-sm-12"
    end
    klass
  end

  # Set sitemap text or link
  def sitemap_text_or_link(sitemap)
    if sitemap.extra_1.present?
      sitemap.extra_1
    elsif sitemap.link.present?
      sitemap_layout = sitemap.parent.layout

      if %w(social).include?(sitemap_layout)
        link_to sitemap.link, target: sitemap.link_target do
          haml_tag :i, class: "fa fa-#{sitemap.text.parameterize}"
        end
      else
        link_to sitemap.text, sitemap.link, target: sitemap.link_target
      end
    else
      link_to sitemap.text, page_path(sitemap), target: sitemap.link_target
    end
  end

  # set option values for each layout
  def get_layout_options(sitemap = '{}', default_opts = '{}')
    opts =
      if sitemap.present? && sitemap&.sitemap_options.present?
        a = sitemap.sitemap_options.collect(&:options).map{ |a| { a['name'] => a['value'] } }
        Hash[*a.collect{ |h| h.to_a }.flatten].reject{ |k, v| v.blank? }
      else
        { }
      end

    default_opts.merge(opts)
  end

  #
  def show_breadcrumbs?
    # show breadcrumb if it's enabled on the ecommerceconfig
    # typically, we do not need to render breadcrumb on homepage & 404 page
    # default is to obey value set in config
    return false if @page.present? && @page.layout == 'homepage'
    return false if response.status == 404

    ENV['BREADCRUMBS']
  end

  #
  def reseller?
    account_signed_in? && current_account.custom_discount_typology == 'reseller'
  end

  #
  def cache_for_locale(cache_key)
    "#{cache_key}/#{I18n.locale.to_s.downcase}"
  end

  #
  def banner_alignment(align)
    arr = { left: 'start', right: 'end', center: 'center' }
    # class_name =
    #   case align
    #   when 'left' then 'start'
    #   when 'right' then 'end'
    #   else 'center'
    "justify-content-#{arr[align.to_sym]}"
  end

  def db_field_to_input(form, model_name, field_name, required=nil)
    # model_name = model_name.camelize
    field_type = model_name.constantize.columns_hash[field_name].type
    field_type =
      case field_type
      when :integer
        form.number_field field_name.to_sym, required: required, class: 'form-control', placeholder: t("#{model_name.pluralize.parameterize}.#{field_name}")
      when :text
        form.text_area field_name.to_sym, required: required, class: 'form-control', placeholder: t("#{model_name.pluralize.parameterize}.#{field_name}")
      else
        form.text_field field_name.to_sym, required: required, class: 'form-control', placeholder: t("#{model_name.pluralize.parameterize}.#{field_name}")
      end
  end

end
