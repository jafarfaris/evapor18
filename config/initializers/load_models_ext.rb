# loads the models_decorations folder, that will contain the extensions for the
# lipsiastore models

# The subfolders of the "app" folder are automatically added to rails'
# eager_load_paths but we need to force-load them

Rails.application.config.to_prepare do
  puts 'Loading app/models_decorations files...'
  # the relative path will be calculated starting from this folder
  ext_root_path = Pathname.new(Rails.root.join('app', 'models_decorations'))

  Dir.glob(Rails.root.join('app', 'models_decorations', '**/*.rb')) do |rb_file|
    require_dependency rb_file
    relative_path = Pathname.new(rb_file).relative_path_from(ext_root_path)

    # auto include the module in the appropriate model
    module_name = relative_path.sub(/#{File.extname(rb_file)}\z/, '').to_s.camelize
    model_name = module_name.gsub(/Ext\z/, '')

    # we allow all files inside app/models_decorations to be loaded
    # regardless it has parent in lipsiaEcommerce or it's a standalone model
    # defined specifically for certain ecommerce
    if model_name.constantize.is_a?(Class)
      model_name.constantize.include(module_name.constantize)
    end

    puts "loaded => #{rb_file}"
  end
end
