# server-based syntax
# ======================
# Defines a single server with a list of roles and multiple properties.
# You can define all roles on a single server, or split them:

# server 'example.com', user: 'deploy', roles: %w{app db web}, my_property: :my_value
# server 'example.com', user: 'deploy', roles: %w{app web}, other_property: :other_value
# server 'db.example.com', user: 'deploy', roles: %w{db}

set :deploy_to, "/home/staging/apps/#{fetch(:application)}"
set :branch, :staging
set :user_name, "staging"
set :ip_server, "staging.lipsiaserver.com"

server fetch(:ip_server), user: fetch(:user_name), roles: %w{web app}

set :rvm_ruby_version, '2.3.4'

set :bundle_without, %w{development test production}.join(' ')

# Defaults to :db role
# set :migration_role, :db
set :migration_role, :migrations_server
# set :migration_servers, -> { primary(fetch(:migration_role)) }
set :migration_servers, -> { roles( fetch(:migration_role) ) }

# Defaults to false
# Skip migration if files in db/migrate were not modified
set :conditionally_migrate, false

after "deploy", "deploy:passenger:restart"
# after "deploy", "deploy:sidekiq:restart"
