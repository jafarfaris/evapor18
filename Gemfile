ruby '2.3.4'

source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '= 4.2.5.2'

# set ENV variables for the project
# always put dotenv-rails on top, after rails
gem 'dotenv-rails'

# Use SCSS for stylesheets
gem 'sass-rails'

# we use bootstrap 4 instead of 3
gem 'bootstrap'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use jquery as the JavaScript library
gem 'jquery-rails'
gem 'jquery-ui-rails'

# Turbolinks makes following links in your web application faster.
# Read more: https://github.com/rails/turbolinks
gem 'turbolinks'

# we use redis for caching data shared between all ecommerces
gem 'redis-rails', '~> 4'

# format site informations (see google structured-data)
gem 'json-ld'

# enables haml
gem 'haml', '~> 5.0'

# easy meta tags
gem 'metamagic'

# easy breadcrumbs
gem 'breadcrumbs_on_rails'

# exceptions managing application
gem 'bugsnag'
gem 'exception_notification'

# font-awesome is a collection of icons ( see http://fontawesome.io/icons/ )
gem 'font-awesome-rails'

# browser and device detection
gem 'browser'

# for pagination
gem 'kaminari'

# we use following gem due to dependency on redirection feature
gem 'rest-client'

# unused gems, we will re-add these gems in case we need it in the future
#
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
# gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
# gem 'sdoc', '~> 0.4.0', group: :doc
# # See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'

  # alternative errors html page
  gem 'better_errors'

  # don't show assets requests in the terminal window
  gem 'quiet_assets'

  # gem to simulate emails in development
  gem 'letter_opener_web', '~> 1.2.0'
end

group :development do
  # debugging
  gem 'active_record_query_trace'
  gem 'awesome_print'
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'

  # Spring speeds up development by keeping your application running in the
  # background. Read more: https://github.com/rails/spring
  # gem 'spring'

  # Deployment with capistrano
  gem 'capistrano', '3.4'
  gem 'capistrano-rails', '~> 1.1'
  gem 'capistrano-rails-logs-tail'
  gem 'capistrano-rvm'

  # at least faster than webrick
  gem 'thin'

  # trace n+1 query
  gem 'bullet'
end

# ==========
# Gems needed to work with Lipsiastore4
# ==========

# Use mysql as the database for Active Record
gem 'mysql2'

# gem used to manage the attachments
gem 'paperclip'

# ruby integration for the Solr search engine
gem 'sunspot_rails'
gem 'sunspot_solr' # optional pre-packaged Solr distribution for use in development

# adds the possibility to set a default value for the attributes of an ActiveRecord object
gem 'default_value_for', '~> 3.0.0'

# Awesome Nested Set is an implementation of the nested set pattern for ActiveRecord models.
gem 'awesome_nested_set'

# enumerator implementation
gem 'enumerize'

# authentication gem
gem 'devise'

# Complete Ruby geocoding solution. (nedded for the accounts to work)
gem 'geocoder'

# ==========
# End lipsiastore4 gems
# ==========
