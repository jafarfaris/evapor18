# Lipsiaecommerce
The foundation for every lipsiastore4-based ecommerce.

# Requirements
- the lipsiastore4 application up and running, with its database
- the .env file specific for your evironment (for example .env.development)

# Conventions
In order to make every ecommerce as standard as possible, we assume the following facts:

### Sitemaps
Each sitemap is rendered and treated differently by its layout, for example a page with the layout "slider" will be rendered as a slider using its images, while a sitemap with the layout "contact" will display a contact form after its text.

We developed a series of conventional layouts that we tend to use through our ecommerces:
##### special sitemaps
- "menu" - the children of this sitemap will be used to populate the main menu, while the sitemap itself will never be visible to the end user.
- "menu2" - same as the menu layout, but used for a secondary menu when necessary
- "footer" - this sitemap holds the content for the footer and the subpages that will be used as needed. The structure of the footer itself is very different from ecommerce to ecommerce but the general rule is that the entire content of the footer is managed by this sitemap and its children.
- "privacy" - this sitemap hosts the wall of text for the privacy conditions and the boring stuffs that nobody will read. Usually a link to this page is present in the footer and the every important form that needs the acceptance of the privacy conditions.
- "TODO condizioni di vendita" (da checkare in fase di acquisto)


##### structural sitemaps
Since there's no real way to manage complex pages (like an homepage) with a single sitemap, we need to use its children to manage each section of the parent page. So to manage an homepage with a slider, some texts, some products and the footer, we need a parent sitemap with multiple children like this:

- homepage
  - slider
    - slide 1 (yep, each slide is a sitemap, because if a banner needs a link, a text and a title, there's no way to add it directly to the image)
    - slide 2
    - slide 3
  - text
  - products

If a sitemap has the special layout **homepage** or **by_children**, we assume that that sitemap is composed by its children.

By default, the available layouts for the childrens are:
- "slider" - every children of this sitemap will be considered as a single banner and only the first image uploaded in each children will be used as a slide.
- "box" - each children holds a generic box with different width based on its layout that can have an image, some text, a link and a title
- "banner" - this layout shouldn't have any children, but will hold an image with a link. The width can be changed by its layout.
- "text" -

# Frontend Developer/QA Test Checklist
Frontend Developer and QA should at least done following tests

## Products

### Product Images
What happens on product detail page when:
- Product has no image
- Product has one image
- Product has multiple images

### Product Pricing

### Product Stock & Availabilities
-
