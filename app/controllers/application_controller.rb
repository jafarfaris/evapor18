#
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :get_location, :set_ecommerce, :set_locale, :set_user

  protected

  # store user location (country ID) in cookies, if it's not presents yet.
  # cookies lasts for 1 day
  def get_location
    if !cookies[:loc]
      Geocoder.configure(ip_lookup: :freegeoip)
      # use fake IP address on development env (Indonesia)
      request.env['HTTP_X_REAL_IP'] = '118.96.249.213' if Rails.env.development?
      cookies[:loc] = {
        value: (request&.location&.country_code || 'IT'),
        expires: 1.day.from_now
      }
    end

    # call cookies inside model using this not-a-good-practice style
    $_COOKIES = cookies
  end

  # stores the ecommerce used for the sitemaps and the translations
  def set_ecommerce
    @current_ecommerce = Ecommerce.active.find(ENV['ECOMMERCE_ID'])
  end

  # lang priority: params if present then default from config.
  # we can't rely on cookie for this case.
  # i'll check for further use on lang on cookie, if we don't use it on certain
  # purposes (other than check which lang is active), i'll delete it
  def set_locale
    # available locales
    # first are default by setting
    locales = @current_ecommerce.locales.reject(&:empty?)

    I18n.default_locale = locales.first

    # by params
    I18n.locale =
      if locales.include?(params[:locale].to_s)
        params[:locale]
      else
        I18n.default_locale
      end.to_sym

    # set cookie lang with the chosen one
    cookies.permanent[:lang] = I18n.locale
  end

  #
  def self.default_url_options
    { locale: I18n.locale }
  end

  #
  def set_user
    $_ACCOUNT = account_signed_in? ? current_account.id : nil
  end
end
