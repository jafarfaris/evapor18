////////////////////////////////////////////////////////////////////////////////
// cookies popup manager
// requires jQuery and js.cookies (https://github.com/js-cookie/js-cookie)
////////////////////////////////////////////////////////////////////////////////


function cookiesPopup() {
  // common variables
  var cookies = {};
  cookies.$close = $("#cookies-message .c-close");
  cookies.$accept = $("#cookies-message .c-accept");

  // simply close cookies popup when clicking on the "X" button
  cookies.$close.click(function(e) {
    $("#cookies-message").fadeOut()
  });

  // when the user accepts, close popup and set a cookie to prevent the popup from reappearing
  cookies.$accept.click(function(e) {
    $("#cookies-message").fadeOut()
    Cookies.set("_ckacc", "1", { expires: 365 });
  });
}

cookiesPopup();
