Bugsnag.configure do |config|
  config.api_key = ENV['BUGSNAG_KEY']
  config.notify_release_stages = ['production', 'staging']

  # sets the application version (certain errors can be silenced until they appear again in a different release)
  # config.app_version = '1.0.0'

  # for the complete documentation see
  # https://docs.bugsnag.com/platforms/ruby/rails/configuration-options/
end
