#
module ParamsTrackable
  extend ActiveSupport::Concern

  included do
    before_action :initialize_tracked_parameters, :track_special_parameters, :set_external_referer
  end

  private

  #
  def initialize_tracked_parameters
    @tracked_parameters = session["_#{ecommerce_name}_trk"] || HashWithIndifferentAccess.new
  end

  # stores any special parameter
  def track_special_parameters
    cookies['__lsh'] = params[:lsh] if params[:lsh].present?
    @tracked_parameters[:lsh] = cookies['__lsh']
    # TODO add other tracking parameters
  end

  #
  def set_external_referer
    referer = URI.parse(request.referer).host rescue nil
    @tracked_parameters[:referer_external] = request.referer if referer.present? && referer != URI.parse(request.url).host
  end
end
