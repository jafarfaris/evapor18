#
class Frontend::CategoriesController < FrontendController
  include FrontendHelper
  include ProductHelper
  include ProductAttributeHelper

  add_breadcrumb I18n.t('products.products'), :categories_path

  #
  def index
    @sitemap = @all_pages.find_by layout: 'root_category'
    defaults = "{
      show_products: 'featured',
      layout: 'card',
      per_page: 10,
      per_row: 4,
      bg_color: '#fff',
      hover_bg_color: '#eee',
      sort: 'latest',
      use_sidebar: true
    }"
    @options = get_layout_options(@sitemap, YAML.safe_load(defaults))
    @products = get_product_list(@options['show_products'], @options['per_page'], @options['sort'])

    if @options['per_page'].to_i > 0
      @products = @products.per(@options['per_page'].to_i)
    end
  end

  #
  def show
    @category = @current_ecommerce.categories.find(params[:id])

    # breadcrumbs
    @category.ancestors.active.order(lft: :asc).each do |cat|
      add_breadcrumb cat.name, category_path(id: cat)
    end

    add_breadcrumb @category.name, category_path(id: @category)

    @sitemap = @all_pages.find_by layout: 'child_category'
    defaults = "{
      show_products: 'featured',
      layout: 'card',
      per_page: 10,
      per_row: 4,
      bg_color: '#fff',
      hover_bg_color: '#eee',
      sort: 'latest',
      use_sidebar: true
    }"
    @options = get_layout_options(@sitemap, YAML.safe_load(defaults))
    @products = @category.products.non_add_ons.page(params[:page])

    if @options['per_page'].to_i > 0
      @products = @products.per(@options['per_page'].to_i)
    end
  end
end
