#
module OrderExt
  extend ActiveSupport::Concern

  included do
    default_scope -> { where(ecommerce_id: ENV['ECOMMERCE_ID']) }
  end
end
