class PriceCalcThreadCleaner

  def initialize app
    @app = app
  end

  def call env
    @app.call(env)
  ensure
    Thread.current[:price_calculator] = nil
  end

end