#
module AccountExt
  extend ActiveSupport::Concern

  attr_accessor :registration_policy

  included do
    # yes, default scope is not a good practice, but in this case is very usefull, we have no intrest in querying all the accounts
    # we just want to work with the accounts for the store that owns the ecommerces we are working with
    # /!\ This default scope also limits devise to authenticate the accounts for this specific store only.
    # That's exacly what we want so keep in mind that removing this default_scope will open a flaw in the devise login
    # (we also allow the store_id otherwise we won't be able to find the store itself)
    default_scope { where(["parent_id = ? OR id = ?", EcommerceConfig.store_id, EcommerceConfig.store_id]) }

    # adds privacy checks
    validates_acceptance_of :registration_policy,  on: :create
    # the role of the accounts should always be of the roles allowed for the ecommerces
    validates :role, inclusion: { in: self::EcommercesRoles }

    # overrides lipsiastore's vat_customer? method that returns true if the corporate_name is present
    # (workaround added for ebagno16)
    def vat_customer?
      role == "vat_customer"
    end

    # overrides lipsiastore's reseller?
    def reseller?
      custom_discount_typology == 'reseller' && ENV['ECOMMERCE_ID'] = ecommerce_id
    end
  end

  # overrides devise find_for_authentication
  # this will allow only the customers to login, not any kind of account
  def self.find_for_authentication(warden_conditions)
    where(email: warden_conditions[:email], role: self::EcommercesRoles).first
  end
end
