#
module ContentHelper
  #
  def parse_content(str = nil, page = nil)
    products = str.scan /#product:\d+#/
    images = str.scan /#image:\d+#/
    videos = str.scan /#youtube:.+#/

    if products.present? || images.present? || videos.present?
      # parse products
      products.each do |product|
        prod_id = product.scan(/#product:(\d+)#/).flatten.first.to_i
        str = str.gsub(product, embed_product(prod_id))
      end

      # parse images
      images.each do |image|
        img_id = image.scan(/#image:(\d+)#/).flatten.first.to_i
        str = str.gsub(image, embed_image(page, img_id))
      end

      # parse youtube
      videos.each do |video|
        url = video.scan(/#youtube:(.+)#/).flatten.first
        str = str.gsub(video, embed_video(url))
      end
    end

    text_to_html(str)
    # str
  end

  #
  def text_to_html(str = nil)
    if str.present?
      str = Nokogiri::HTML::DocumentFragment.parse(str).to_html
      str = str
        .gsub(/\r\n\r\n/, '</p><p>')
        .gsub(/\r\n/, '<br>')
        .gsub(/<br>[\s$]*<li>/, '<li>')
        .gsub(/<p>[\s$]*<\/p>/, '')
        .html_safe

      content_tag :p, str
    end
  end

  #
  def embed_product(prod_id)
    prod = Product.find(prod_id)
    render('frontend/pages/parts/embed_product', product: prod) if prod.present?
  end

  #
  def embed_image(page, image_id)
    image = page.images.normals[image_id.to_i - 1]

    if image.present?
      content_tag :div, class: 'content-image' do
        image_tag image&.url
      end
    end
  end

  #
  def embed_video(vid_id)
    "<div class=\"video-player\"><iframe src=\"//youtube.com/embed/#{vid_id}\" frameborder=\"0\" gesture=\"media\" allowfullscreen></iframe></div>"
  end
end
