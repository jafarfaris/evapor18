module CountryExt
  extend ActiveSupport::Concern

  included do
    # This array can be filled with "banned" countries, that will be ignored by the whole application
    DisabledCountries = [
      # "Afghanistan"
    ]

    Eu = [
      "Austria", "Belgium", "Bulgaria", "Croatia", "Cyprus", "Czech Republic", "Denmark", "Estonia", "Finland", "France", "Germany", "Greece", 
      "Hungary", "Ireland", "Italy", "Latvia", "Lithuania", "Luxembourg", "Malta", "Netherlands", "Poland", "Portugal", "Romania" "Slovakia", 
      "Slovenia", "Spain", "Sweden", "United Kingdom"
    ]



    default_scope -> { where.not({name: Country::DisabledCountries}) }
  end

end