# sets various helpful constants

raise 'The constant EcommerceConfig is not defined, this means that the lipsiastore4 clients modules are not loaded.' if EcommerceConfig.nil?

port_string = ENV['PORT'] == 80 || ENV['PORT'].blank? ? '' : ":#{ENV['PORT']}"

EcommerceConfig.host_with_port = "#{ENV['HOST']}#{port_string}"
EcommerceConfig.store_id = Ecommerce.find(ENV['ECOMMERCE_ID']).store_id rescue nil
