# this custom failure class manages the redirects on failed logins

class DeviseCustomFailure < Devise::FailureApp

  def redirect_url 
    # specific fail path when logging from the cart
    if request[:controller] =~ /frontend\/cart\//
      cart_registrations_login_or_register_path
    else
      super
    end
  end 
  

  def respond 
    if http_auth? 
      http_auth 
    else 
      redirect 
    end 
  end 

end 


# adds the failure to warden
Devise.warden do |manager|
  manager.failure_app = DeviseCustomFailure
end