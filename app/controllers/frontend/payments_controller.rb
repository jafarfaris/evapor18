class Frontend::PaymentsController < FrontendController

  # order payment without authentication
  def external_payment
    # params[:custom_id]
    # params[:total] are non-influent to the payment page, the total is used as a "security measure" to see if it matches the order custom_id
    @order = Order.where(custom_id: params[:custom_id], total_actual: params[:total]).first
    # where returns an empty AR object, in case the order doesn't exist we want a 404 page
    raise ActiveRecord::RecordNotFound unless @order.present?

    if (payment = @current_ecommerce.payments.find(params[:payment_id]) ).present?
      @order.update(payment_id: payment.id)
    end
    # redirect to the account's payment page if the user is already logged in 
    # (that account payment page is more complete and allows the user to update the payment method)
    return redirect_to payment_accounts_order_path(id: @order) if @current_account.present? && @current_account.orders.include?(@order)
  end

end