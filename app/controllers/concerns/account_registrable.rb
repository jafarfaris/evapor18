#
module AccountRegistrable
  extend ActiveSupport::Concern

  included do
    before_action :set_min_pw_length
  end

  private

  #
  def set_min_pw_length
    @minimum_password_length = Devise.password_length.min
  end

  # def to create the account, this is plainly taken from the devise controller
  # https://github.com/plataformatec/devise/blob/master/app/controllers/devise/registrations_controller.rb
  def create_account
    build_resource(sign_up_params)

    resource.save
    yield resource if block_given?
    if resource.persisted?
      if resource.active_for_authentication?
        set_flash_message! :notice, :signed_up
        sign_up(resource_name, resource)
        respond_with resource, location: after_sign_up_path_for(resource)
      else
        set_flash_message! :notice, :"signed_up_but_#{resource.inactive_message}"
        expire_data_after_sign_in!
        respond_with resource, location: after_inactive_sign_up_path_for(resource)
      end
    else
      clean_up_passwords resource
      set_minimum_password_length
      render @failure_view
    end
  end

  # overrides devise method to add some @additional_attributes to the account
  def sign_up_params
    attrs = devise_parameter_sanitizer.sanitize(:sign_up)
    attrs.merge!(@additional_resource_params) if @additional_resource_params
    attrs
  end

  #
  def configure_permitted_parameters
    if @additional_resource_params
      case @additional_resource_params[:role]
      when 'vat_customer'
        devise_parameter_sanitizer.permit(:sign_up, keys:
          %i[email name surname password password_confirmation piva cfis phone
             registration_policy corporate_name telephone_prefix telephone
             phone_prefix address address_2 country province city cap])
      when 'cliente'
        devise_parameter_sanitizer.permit(:sign_up, keys:
          %i[email name surname password password_confirmation telephone phone
             registration_policy cfis birthday sex telephone_prefix phone_prefix
             address address_2 country province city cap])
      when 'basic_user'
        devise_parameter_sanitizer.permit(:sign_up, keys:
          %i[email name surname password password_confirmation
             registration_policy])
      end
    end
  end
end
