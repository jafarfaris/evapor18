#
class Frontend::PagesController < FrontendController
  include FrontendHelper
  include ProductHelper
  include ProductAttributeHelper

  #
  def index
    @page = @all_pages.find_by_layout 'homepage'
    render 'frontend/pages/show'
  end

  #
  def show
    @page =
      if params[:id].present?
        @all_pages.find(params[:id])
      else
        @all_pages.find_by_layout('homepage')
      end
    # return redirect_to page_path(@page) if request.path != page_path(@page)

    # # sets active page in the menu (if the current page is a children of the menu page)
    # if @page.parent == @menu # @page is a direct children of the menu
    #   @current_menu = @page.text
    # else # @page is a grandchildren of the menu
    #   @current_menu = @page.ancestors.select{|sitemap| sitemap.parent == @menu}.compact.map(&:text).first
    # end

    # print breadcrumbs
    add_breadcrumb(@page.text)

    # # render template in base of page
    # template_for(@page)
  end

  private

  # compose current page breadcrumbs using the gem
  def breadcrumbs_for(sitemap)
    sitemap.ancestors.menu_enable.each do |ancestor|
      link = ancestor.link
      link = page_path(ancestor) if link.blank?
      add_breadcrumb(ancestor.text, link)
    end

    add_breadcrumb(sitemap.text)
  end

  # render specific template for passed sitemap,
  def template_for(sitemap); end
end
