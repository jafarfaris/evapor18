// binds the plus and minus buttons to change the cart quantity
// also validates the input field, it should always be an integer bigger than 0


$(function(){
  var $inputsBox = $(".item-quantity");

  // binds the function to every cart item
  $inputsBox.each(function(){
    var $this = $(this);
    var $input = $this.find(".quantity-input input");
    var $plus = $this.find(".quantity-input .increment");
    var $minus = $this.find(".quantity-input .decrement");

    var initialQuantity = $input.val();


    function changeQuantity(amount){
      var value = Number($input.val());
      value = value + amount;

      if ( validateValue(value) ){
        $input.val(value);
        $input.trigger("change");
      }
    }


    // performs the necessary validations on the input value
    // returns true if the value is a valid integer bigger than 0
    function validateValue(value){
      var value = Number(value);

      if ( isNaN(value) )
        return false;

      if ( value < 1 )
        return false;

      return true;
    }

    // binds buttons
    $plus.click(function(e){
      e.preventDefault();
      changeQuantity(1);
    });

    $minus.click(function(e){
      e.preventDefault();
      changeQuantity(-1);
    });



    $input.on("propertychange change click keyup input paste", onKeyboadrdInput);


    function onKeyboadrdInput(){
      var value = $(this).val();
      // if the value is not a valid number
      // fallback to the initial value
      // if also the initial value is not valid for any reason, fallback to 1
      if ( !validateValue(value) ){
        if ( !isNaN(Number(initialQuantity)) ){
          value = initialQuantity;
        } else {
          value = 1;
        }
      }

      $input.val(value);
    }

  });


  ///////////////////////////////////
  // Cart recap button toggle
  ///////////////////////////////////

  $(".recap-bar").click(function(e){
    e.preventDefault();
    $("body").toggleClass("cart-recap-visible");
  })

});
