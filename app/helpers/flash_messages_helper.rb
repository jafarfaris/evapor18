module FlashMessagesHelper

  # if You are passing a single string to the method flash without the second argument, we assume that that string is a text and not the title of the flash message
  # If You want to explicitly use it as a title you need to pass en empty string as the second argument
  #
  # for example:
  # =flash_success("Good work")                           <- in this case "Good work" will be interpreted as the flash message
  # =flash_success("Good work", "")                       <- in this case "Good work" will be interpreted as the title and the flash message will be left empty
  # =flash_success("Good work", "This is the message")    <- in this case "Good work" will be interpreted as the title and the flash message will be left empty
  # =flash_success("Good work", ["This is the first item", "second one!"])    <- in this case the array will be auto interpreted as unordered list, each item will be a <li>
  #                                                                              ONLY if the array has two or more items, a single item will be interpreted as a simple message


  def flash_messages
    flash.map{|k, messages| flash_message(k, messages)}.join("").html_safe
  end

  # def flash_primary(title, message = nil)
  #   options = title_or_message(title, message)
  #   options[:type] = "primary"
  #   flash_message_html(options)
  # end



  # def flash_success(title, message = nil)
  #   options = title_or_message(title, message)
  #   options[:type] = "success"
  #   flash_message_html(options)
  # end



  # def flash_info(title, message = nil)
  #   options = title_or_message(title, message)
  #   options[:type] = "info"
  #   flash_message_html(options)
  # end


  # def flash_warning(title, message = nil, dismissable = false)
  #   options = title_or_message(title, message)
  #   options[:type] = 'warning'
  #   options[:dismissable] = dismissable
  #   flash_message_html(options)
  # end



  # def flash_danger(title, message = nil)
  #   options = title_or_message(title, message)
  #   options[:type] = "danger"
  #   flash_message_html(options)
  # end

  # TODO: we'll remove other helper an use only this helper in the future
  def flash_message(type, title, message = nil, dismissable = true)
    options = title_or_message(title, message)
    options[:type] = bootstrap_class_for(type)
    options[:dismissable] = dismissable
    flash_message_html(options)
  end


  def bootstrap_class_for(type)
    { error: 'danger', errors: 'danger', success: 'success', warning: 'warning' }[type.to_sym] || 'info'
  end

  # basic flash message builder method
  # the options are passed as an hash, this way they can be easily extended with additions upon necessity
  def flash_message_html(options)
    title = options.delete :title
    message = options.delete :message
    type = options.delete :type

    html = <<-HTML
    <div class="alert alert-#{type} alert-dismissable" role="alert">
    HTML

    html += '<button class="close" type="button" data-dismiss="alert" aria-hidden="true">&times;</button>' if options[:dismissable].to_s == 'true'
    # title
    html += "<h4>#{title}</h4>" if title.present?

    # message
    # if the message is an array, automatically create an ul
    if message.is_a?(Array) && message.size > 1
      html += "<ul>"
      html += message.map{|mex| "<li>#{mex}</li>"}.join("")
      html += "</ul></div>"
    else
      message = message.first if message.is_a?(Array)
      html += "#{message}</div>"
    end

    html.html_safe
  end


  private

    # returns an hash with the appropriate configurations for the title and the message
    def title_or_message(title, message)
      if message.nil?
        {title: nil, message: title}
      else
        {title: title, message: message}
      end
    end

end
