
#
module ProductExt
  extend ActiveSupport::Concern

  included do
    include OgImageable

    # weird attribute naming here
    # visible = purchasable
    # invisible = not purchasable
    scope :active, -> { where(active: true) }
    scope :base, -> { active.joins(:ecommerces).where(store_id: EcommerceConfig.store_id, ecommerces_products: { ecommerce_id: ENV['ECOMMERCE_ID'] }).distinct }
    scope :add_ons, -> { unscoped.base.where(products: { layout: 'add-on' }) }
    scope :non_add_ons, -> { where.not(products: { layout: 'add-on' }) }
    default_scope { base }

    translate_fields :name, :description, :description_short

    #
    def to_param
      "#{id}-#{name.to_s.downcase.gsub(/[^a-z0-9]+/, '-').gsub(/-+$/, '').gsub(/^-+$/, '')}"
    end
  end

  #
  def available?
    qty > 0
  end

  #
  def availability
    if available?
      'available'
    elsif pre_orderable
      'preorder'
    elsif date_arrival.present? && date_arrival > Date.current
      'restocking'
    else
      'unavailable'
    end
  end

  #
  def purchasable?
    ['available', 'preorder', 'restocking'].include?(availability) && active?
  end

  #
  def attribute_options
    Detail.joins(:product_versions).where(product_versions: { id: product_version_id }).uniq
  end

  # alternative versions of certain product
  def alternative_versions
    versions.where.not(id: id).uniq
  end

  # rather a hack :) (no point to buy 0 item)
  def minimum_quantity
    minimal_quantity < 1 ? 1 : minimal_quantity
  end

  #
  module ClassMethods
    def price_calculator=(calculator)
      Thread.current[:price_calculator] = calculator
    end

    def price_calculator
      Thread.current[:price_calculator]
    end
  end
end
