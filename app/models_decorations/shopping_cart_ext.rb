#
module ShoppingCartExt
  extend ActiveSupport::Concern

  included do
    # yes, default scope is generally not a good practice, but in this case is very usefull as we shouldn't even consider any shopping cart that doesn't belong to this ecommerce
    default_scope -> { where(ecommerce_id: ENV['ECOMMERCE_ID']) }

    validate :has_payment

    # overide existing method
    def total_products_with_vat
      products.map{ |i| i.total_with_vat }.sum
    end

    #
    def total_services_with_vat
      services.map{ |i| i.total_with_vat }.sum
    end

    #
    def payment_price_with_vat
      payment.present? ? payment.price : 0
    end
  end

  attr_accessor :validate_payment

  # Overrides the delivery method to return a fallback
  def delivery
    del = ecommerce.deliveries.find_by_id(delivery_id) # search the delivery only in the ecommerce
    del.present? ? del : possible_deliveries.cheapest
  end

  # returns the deliveries available to choose based on the account address
  def possible_deliveries
    # if the cart has a specific shipping address assigned
    if shipping_address.present? && shipping_address.country.present?
      ecommerce.deliveries.for_country(shipping_address.country)

    # if there's a customer assigned, use the customer billing address
    elsif customer.present? && customer.country.present?
      ecommerce.deliveries.for_country(customer.country)

    # cheapest delivery fallback
    else
      ecommerce.deliveries
    end
  end

  private
    # adds a different error
    def has_payment
      errors.add(:payment_id, :blank_blocking) if validate_payment && payment.blank?
    end
end
