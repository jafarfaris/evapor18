class Frontend::AddressesController < FrontendController

  # there's no need to get the provinces since we assume that the only provinces are the italians

  # def provinces
  #   @country = Country.find(params[:country_id])
  #   @provinces = @country.provinces
  # end


  def cities
    @province = Province.find_by_full_description(params[:province_name])
    @cities = @province.cities

    respond_to do |format|
      format.html{ render layout: false}
    end
  end


  def suggest_cap
    @province = Province.find_by_full_description(params[:province_name])
    @city = @province.cities.find_by_name(params[:city_name]) if @province 

    if @city
      render text: @city.cap
    else
      render nothing: true, status: 404
    end


  end


end