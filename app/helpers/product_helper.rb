#
module ProductHelper
  #
  def quantity_field_for(product, val = nil, field_name = :quantity)
    qty = product.qty
    min_purchase_qty = product.min_purchase_qty
    multiples_purchase_qty = product.fixed_quantity
    max_purchase_qty = product.pre_orderable? ? 100000 : qty

    # if minimum purchase qty less than multiples purchase quantity, then set default value to multiple purchase qty
    min = min_purchase_qty < multiples_purchase_qty ? multiples_purchase_qty : min_purchase_qty

    if product.purchasable?
      content_tag :div, class: 'input-group' do
        number_field_tag field_name, (val || min), min: min, max: max_purchase_qty, step: multiples_purchase_qty, class: 'quantity-input form-control', id: "quantity-#{product.id}"
      end
    end
  end

  #
  def get_product_list(kind, limit = nil, sort = 'random')
    # put order inside condition as we'll add more display_type with different
    # order option in the future
    conditions = {  }
    conditions =
      case kind
      when 'featured'
        { homepage: true }
      when 'related'
        { id: @product.correlations.collect(&:id) } if @product.present?
      when 'last_seen'
        # if user logged in, get from his recently_viewed_products_ids
        # otherwise try to get from his cookies
        if account_signed_in?
          recent_ids = current_account.recently_viewed_products_ids.to_s.split(',').map(&:to_i).uniq
          { id: recent_ids.select{ |a| a != @product&.id } }
        elsif cookies[:__rwp].present?
          recent_ids = cookies[:__rwp].to_s.split(',').map(&:to_i).uniq
          { id: recent_ids.select{ |a| a != @product&.id } }
        end
      when 'same_category'
        if @product.present? && @categories.present?
          prod_ids = @first_category.products.where.not(id: @product.id).collect(&:id)
          { id: prod_ids }
        else
          # this should returns nothing
          { id: nil }
        end
      when 'new'
        { id: @current_ecommerce.products.where(new_product: true).pluck(:id) }
      when 'show_on_root_category'
        { id: @current_ecommerce.products.where(category_show_up: true).pluck(:id) }
      when 'recommended'
        { id: @current_ecommerce.products.where(recommended: true).pluck(:id) }
      else
        # all product
        { id: @current_ecommerce.products.pluck(:id) }
      end

    prods = Product.non_add_ons.includes(:international_prices).where(conditions).reorder('RAND()')

    all_attributes.each do |att|
      if params[att.name&.parameterize].present?
        prods = prods.joins(:details, :detail_products).where('details.name LIKE ? AND detail_products.value = ?', "%#{att.name&.parameterize}%", params[att.name&.parameterize])
      end
    end

    prods =
      case sort
      when 'random'
        prods.reorder 'RAND()'
      else
        prods.reorder created_at: :desc
      end

    if limit.to_i > 0
      prods = prods.limit(limit.to_i)
    end

    prods.page(params['page'])
  end
end
