#
class AccountsController < FrontendController
  before_action :authenticate_account!, :load_account_vars
  add_breadcrumb I18n.t('home'), :root_path
  add_breadcrumb I18n.t('my_account'), :accounts_dashboard_path

  #
  def dashboard
    @account_action = 'dashboard'
    @latest_orders = @account_orders.limit(3)
  end

  # basic edit action
  def edit
    add_breadcrumb t('edit_data'), edit_account_path
    @account_action = 'edit'
  end

  # edit a vat account (this is also used to pass the user from a cliente to a
  # vat account)
  def edit_vat
    add_breadcrumb t('edit_data'), edit_vat_account_path
    @account_action = 'edit'
  end

  #
  def update
    @account_action = 'edit'
    @current_account.role = 'cliente'
    flash.now[:account_success] = t('account_saved') if @current_account.update(account_permitted_params)

    render 'edit'
  end

  #
  def update_vat
    @account_action = 'edit'
    @current_account.role = 'vat_customer'
    flash.now[:account_success] = t('account_saved') if @current_account.update(account_permitted_params)

    render 'edit_vat'
  end

  #
  def edit_password
    add_breadcrumb t('change_password'), edit_password_account_path
    @account_action = 'edit_password'
  end

  #
  def update_password
    @account_action = 'edit_password'

    if @current_account.update_with_password(account_password_permitted_params)
      # if the account password is updated, the user is automatically logged out
      # and the edit action won't be reached but if the password passed is
      # blank, the account won't be updated and he will see the edit form again
      return redirect_to edit_password_account_path
    end

    render 'edit_password'
  end

  private

  #
  def load_account_vars
    # load the orders made by the current account on the ecommerces managed by
    # this application
    @account_orders = @current_account.orders.order(created_at: :desc)
  end

  #
  def account_permitted_params
    if @current_account.vat_customer?
      params.require(:account).permit(
        %i[name surname corporate_name piva cfis telephone_prefix telephone
           phone_prefix phone address address_2 country province city cap]
      )
    else
      params.require(:account).permit(
        %i[name surname cfis address address_2 country province city cap
           telephone phone telephone_prefix phone_prefix birthday sex]
      )
    end
  end

  #
  def account_password_permitted_params
    params.require(:account).permit(%i[password password_confirmation current_password])
  end
end
